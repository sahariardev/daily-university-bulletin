CREATE TABLE role (

  id         INT PRIMARY KEY AUTO_INCREMENT,
  title      VARCHAR(100)                                                   NOT NULL,
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP                             NOT NULL,
  updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL
);
CREATE TABLE university (

  id              INT PRIMARY KEY AUTO_INCREMENT,
  title           VARCHAR(100)                                                   NOT NULL,
  address         VARCHAR(1000)                                                  NOT NULL,
  website_url     VARCHAR(300)                                                   NOT NULL,
  foundation_year DATETIME                                                       NOT NULL,
  lat             INT                                                            NOT NULL,
  lon             INT                                                            NOT NULL,
  contact_number  VARCHAR(13)                                                    NOT NULL,
  created_by      INT,
  created_at      DATETIME DEFAULT CURRENT_TIMESTAMP                             NOT NULL,
  updated_at      DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL
);
CREATE TABLE user (

  id             INT PRIMARY KEY AUTO_INCREMENT,
  first_name     VARCHAR(100)                                                   NOT NULL,
  last_name      VARCHAR(100)                                                   NOT NULL,
  user_email     VARCHAR(100)                                                   NOT NULL UNIQUE,
  password       VARCHAR(100)                                                   NOT NULL,
  account_status VARCHAR(100)                                                   NOT NULL,
  address        VARCHAR(1000),
  role_id        INT                                                            NOT NULL,
  university_id  INT                                                            NOT NULL,
  updated_by     INT,
  created_at     DATETIME DEFAULT CURRENT_TIMESTAMP                             NOT NULL,
  updated_at     DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  FOREIGN KEY (university_id) REFERENCES university (id),
  FOREIGN KEY (role_id) REFERENCES role (id),
  FOREIGN KEY (updated_by) REFERENCES user (id)

);

CREATE TABLE tag (

  id         INT PRIMARY KEY AUTO_INCREMENT,
  title      VARCHAR(100)                                                   NOT NULL,
  created_by INT                                                            NOT NULL,
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP                             NOT NULL,
  updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  FOREIGN KEY (created_by) REFERENCES user (id)
);

CREATE TABLE news (

  id             INT PRIMARY KEY AUTO_INCREMENT,
  title          VARCHAR(300) UNIQUE                                            NOT NULL,
  image          MEDIUMBLOB,
  publish_date   DATETIME,
  end_date       DATETIME,
  current_status VARCHAR(100)                                                   NOT NULL,
  priority       INT,
  body           TEXT                                                           NOT NULL,
  created_by     INT                                                            NOT NULL,
  published_by   INT,
  university_id  INT                                                            NOT NULL,
  created_at     DATETIME DEFAULT CURRENT_TIMESTAMP                             NOT NULL,
  updated_at     DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  FOREIGN KEY (university_id) REFERENCES university (id),
  FOREIGN KEY (created_by) REFERENCES user (id),
  FOREIGN KEY (published_by) REFERENCES user (id)
);
CREATE TABLE news_tags (

  news_id INT NOT NULL,
  tag_id  INT NOT NULL,
  UNIQUE (news_id, tag_id),
  FOREIGN KEY (news_id) REFERENCES news (id),
  FOREIGN KEY (tag_id) REFERENCES tag (id)
);
CREATE TABLE news_comments (

  id         INT PRIMARY KEY AUTO_INCREMENT,
  body       VARCHAR(500)                                                   NOT NULL,
  created_by INT,
  news_id    INT,
  created_at DATETIME DEFAULT CURRENT_TIMESTAMP                             NOT NULL,
  updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL,
  FOREIGN KEY (news_id) REFERENCES news (id),
  FOREIGN KEY (created_by) REFERENCES user (id)
);

ALTER TABLE university
ADD CONSTRAINT FKpv67t70xy1phe5daa320r025l
FOREIGN KEY (created_by)
REFERENCES user (id);