INSERT INTO role ('title') VALUES ('SUPER_ADMIN');

INSERT INTO role ('title') VALUES ('ADMIN');

INSERT INTO role ('title') VALUES ('AUTHOR');

INSERT INTO role ('title') VALUES ('SUBSCRIBER');

INSERT INTO university ('title', 'address', 'website_url', 'foundation_year', 'lat', 'lon', 'contact_number')
VALUES ('BRAC UNIVERSITY', 'a', 'a', '2017-12-05', 'asd', 'sada', '1234');

UPDATE user
SET account_status = "ACTIVATED"
WHERE id = 1;

UPDATE user
SET role_id = 1
WHERE id = 1;
