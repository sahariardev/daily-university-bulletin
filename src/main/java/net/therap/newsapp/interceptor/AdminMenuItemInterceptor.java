package net.therap.newsapp.interceptor;

import net.therap.newsapp.cmd.AdminMenuItemCmd;
import net.therap.newsapp.constant.ApplicationConstant;
import net.therap.newsapp.domain.User;
import net.therap.newsapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static net.therap.newsapp.constant.URL.*;

/**
 * @author sahariar.alam
 * @since 10/14/19
 */

public class AdminMenuItemInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private UserService userService;

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response,
                           Object handler,
                           ModelAndView modelAndView) throws Exception {
        HttpSession session = request.getSession();
        if (Objects.nonNull(session.getAttribute(ApplicationConstant.USER)) && Objects.nonNull(modelAndView)) {
            User user = userService.findByEmail(((User) session.getAttribute("user")).getEmail());
            if (!user.isActivated()) {
                return;
            }
            AdminMenuItemCmd profile = new AdminMenuItemCmd("Profile", PROFILE);
            AdminMenuItemCmd users = new AdminMenuItemCmd("Users", USER);
            AdminMenuItemCmd universities = new AdminMenuItemCmd("Universities", UNIVERSITY);
            AdminMenuItemCmd tags = new AdminMenuItemCmd("Tags", TAGS);
            AdminMenuItemCmd news = new AdminMenuItemCmd("News", NEWS);
            List<AdminMenuItemCmd> items = new ArrayList<>();
            items.add(profile);
            if (user.isSuperAdmin()) {
                items.add(users);
                items.add(universities);
                items.add(tags);
                items.add(news);
            } else if (user.isAdmin()) {
                items.add(tags);
                items.add(news);
            } else if (user.isAuthor()) {
                items.add(news);
            }
            modelAndView.addObject("menuItems", items);
        }
    }
}
