package net.therap.newsapp.constant;

/**
 * @author sahariar.alam
 * @since 9/30/19
 */
public interface URL {

    String AUTHENTICATED_PREFIX = "/a";

    String PROFILE = AUTHENTICATED_PREFIX + "/profile";

    String USER = AUTHENTICATED_PREFIX + "/users";

    String UNIVERSITY = AUTHENTICATED_PREFIX + "/universities";

    String TAGS = AUTHENTICATED_PREFIX + "/tags";

    String NEWS = AUTHENTICATED_PREFIX + "/news";

    String PUBLIC_PREFIX = "/p";

    String AUTH = PUBLIC_PREFIX + "/auth";

    String HOME = PUBLIC_PREFIX + "/home";

    String COMMON_NEWS_URL = PUBLIC_PREFIX + "/news/{id}";

    String NEWS_URL = PUBLIC_PREFIX + "/news/";

    String COMMON_UNIVERSITY_URL = PUBLIC_PREFIX + "/university/{id}";

    String SIGN_UP_FORM = "/create";

    String REDIRECT = "redirect:";

    String USER_CHANGE_INFO = "/changeinfo";

    String USER_CHANGE_PASSWORD = "/changepassword";

    String CHANGE_USER_ROLE_STATUS = "all/change";

    String EDIT = "/edit/";

    String PATH_VARIABLE_ID = "/{id}";

    String UNIVERSITY_CREATE = "/create";

    String UPDATE = "/edit/{id}";

    String NEWS_CREATE = "/create";

    String ROOT = "/";

    String LOGIN = "/login";

    String LOGOUT = "/logout";

    String SAVE_COMMENT = "/{id}/comment";

    String DELETE_COMMENT = "/{id}/comment/delete";
}
