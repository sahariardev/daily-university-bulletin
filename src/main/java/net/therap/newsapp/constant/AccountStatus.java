package net.therap.newsapp.constant;

/**
 * @author sahariar.alam
 * @since 9/29/19
 */
public enum AccountStatus {

    PENDING("Pending"),

    ACTIVATED("Activated"),

    BANNED("Banned");

    private String displayName;

    AccountStatus(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
