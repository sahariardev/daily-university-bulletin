package net.therap.newsapp.constant;

/**
 * @author sahariar.alam
 * @since 10/2/19
 */
public enum PostStatus {

    PUBLISHED("Published"),

    DRAFT("Draft"),

    REQUEST_FOR_PUBLISH("Request for publish");

    private String displayName;

    PostStatus(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
