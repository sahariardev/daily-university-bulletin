package net.therap.newsapp.constant;

/**
 * @author sahariar.alam
 * @since 10/1/19
 */
public enum RoleType {

    SUPER_ADMIN("Super Admin"),

    ADMIN("Admin"),

    AUTHOR("Author"),

    SUBSCRIBER("Subscriber");

    private String displayName;

    RoleType(String displayName) {
        this.displayName = displayName;
    }

    public String getDisplayName() {
        return displayName;
    }
}
