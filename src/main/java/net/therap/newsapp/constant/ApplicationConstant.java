package net.therap.newsapp.constant;

/**
 * @author sahariar.alam
 * @since 10/21/19
 */
public interface ApplicationConstant {

    int FIRST_PAGE = 1;

    int LIMIT_PER_PAGE = 10;

    String USER = "user";
}
