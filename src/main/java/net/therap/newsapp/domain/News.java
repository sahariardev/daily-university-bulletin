package net.therap.newsapp.domain;

import net.therap.newsapp.constant.PostStatus;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author sahariar.alam
 * @since 10/2/19
 */
@Entity
@Table(name = "news")
@NamedQueries({
        @NamedQuery(name = "News.findAll", query = "FROM News  ORDER BY updated desc, id DESC"),

        @NamedQuery(name = "News.findByUniversity", query = "FROM News WHERE university = :university " +
                "ORDER BY updated DESC"),

        @NamedQuery(name = "News.findByStatus", query = "FROM News WHERE status = :currentStatus"),

        @NamedQuery(name = "News.findByUser", query = "FROM News WHERE createdBy = :user ORDER BY updated DESC"),

        @NamedQuery(name = "News.findByUniversityAndStatus", query = "FROM News WHERE university = :university " +
                "AND status = :currentStatus ORDER BY updated DESC"),

        @NamedQuery(name = "News.findByUserAndStatus", query = "FROM News WHERE createdBy = :user " +
                "AND status = :currentStatus ORDER BY updated DESC"),

        @NamedQuery(name = "News.findTopNews", query = "FROM News WHERE status = :currentStatus " +
                "AND publishDate <= :date AND endDate > :date ORDER BY priority DESC"),

        @NamedQuery(name = "News.search", query = "FROM News WHERE (title LIKE :keyword or body LIKE :keyword) " +
                "AND (status = :currentStatus " +
                "AND publishDate <= :date AND endDate > :date) ORDER BY priority DESC"),

        @NamedQuery(name = "News.findPublishedNewsByUniversity", query = "FROM News WHERE university = :university " +
                "AND status = :currentStatus " +
                "AND publishDate <= :date AND endDate > :date ORDER BY priority DESC"),
        @NamedQuery(name = "News.findByTitle", query = "FROM News WHERE title = :title "),

        @NamedQuery(name = "News.findByTitleAndNotEqualId", query = "FROM News WHERE title = :title AND id != :id"),
})
public class News extends Persistent {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(unique = true, nullable = false)
    @NotNull(message = "{app.news.title}")
    @Size(max = 300, message = "{app.news.title}")
    private String title;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] image;

    @Column(nullable = false)
    @NotNull(message = "{app.news.body}")
    private String body;

    @Column(name = "publish_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date publishDate;

    @Column(name = "end_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;

    @Column(name = "current_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private PostStatus status;

    private int priority;

    @ManyToOne
    @JoinColumn(name = "created_by", nullable = false, updatable = false)
    private User createdBy;

    @ManyToOne
    @JoinColumn(name = "university_id", nullable = false)
    private University university;

    @ManyToOne
    @JoinColumn(updatable = false, name = "published_by")
    private User publishedBy;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "news_id")
    private List<Comment> comments;

    @ManyToMany
    @JoinTable(
            name = "news_tags",
            joinColumns = {@JoinColumn(name = "news_id")},
            inverseJoinColumns = {@JoinColumn(name = "tag_id")}
    )
    @NotNull(message = "{app.select.one.tag}")
    private List<Tag> tags;

    @Transient
    private MultipartFile imageFile;

    public News() {
        this.comments = new ArrayList<>();
        this.status = PostStatus.DRAFT;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public MultipartFile getImageFile() {
        return imageFile;
    }

    public void setImageFile(MultipartFile imageFile) {
        this.imageFile = imageFile;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public PostStatus getStatus() {
        return status;
    }

    public void setStatus(PostStatus status) {
        this.status = status;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public University getUniversity() {
        return university;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    public User getPublishedBy() {
        return publishedBy;
    }

    public void setPublishedBy(User publishedBy) {
        this.publishedBy = publishedBy;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public boolean isNew() {
        return id == 0;
    }

    public boolean isPublished() {
        return status.equals(PostStatus.PUBLISHED);
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    public void addNewComment(Comment comment) {
        comments.add(comment);
    }

    public void removeComment(Comment comment) {
        comments.remove(comment);
    }

    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", body='" + body + '\'' +
                ", publishDate=" + publishDate +
                ", endDate=" + endDate +
                ", status=" + status +
                ", priority=" + priority +
                ", createdBy=" + createdBy +
                ", university=" + university +
                ", publishedBy=" + publishedBy +
                ", comments=" + comments +
                ", tags=" + tags +
                ", imageFile=" + imageFile +
                '}';
    }
}
