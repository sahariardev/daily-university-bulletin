package net.therap.newsapp.domain;

import net.therap.newsapp.constant.AccountStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author sahariar.alam
 * @since 9/29/19
 */
@Entity
@Table(name = "user")
@NamedQueries({
        @NamedQuery(name = "User.findAll", query = "FROM User"),

        @NamedQuery(name = "User.findByEmail", query = "FROM User WHERE email = :email"),

        @NamedQuery(name = "User.findByRole", query = "FROM User WHERE role = :role"),

        @NamedQuery(name = "User.findByStatus", query = "FROM User WHERE accountStatus = :status"),

        @NamedQuery(name = "User.findByRoleAndStatus",
                query = "FROM User WHERE accountStatus = :status AND role = :role")
})
public class User extends Persistent {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "first_name", nullable = false)
    @NotNull(message = "{app.not.empty}")
    @Size(min = 1, max = 100, message = "{app.first.name.length}")
    private String firstName;

    @Column(name = "last_name", nullable = false)
    @NotNull(message = "{app.canNotBeEmpty}")
    @Size(min = 1, max = 100, message = "{app.last.name.length}")
    private String lastName;

    @Column(nullable = false)
    @Size(min = 4, max = 32, message = "{app.password.length.message}")
    private String password;

    @Column(name = "user_email", nullable = false)
    @NotNull(message = "{app.not.empty}")
    private String email;

    @Column(name = "account_status", nullable = false)
    @Enumerated(EnumType.STRING)
    private AccountStatus accountStatus;

    @Size(max = 1000)
    private String address;

    @ManyToOne
    @JoinColumn(name = "university_id", nullable = false)
    @NotNull(message = "{app.user.university}")
    private University university;

    @OneToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id", nullable = false)
    private Role role;

    @ManyToOne
    @JoinColumn(name = "updated_by")
    private User updatedBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public University getUniversity() {
        return university;
    }

    public User getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(User updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setUniversity(University university) {
        this.university = university;
    }

    public boolean isActivated() {
        return getAccountStatus().equals(AccountStatus.ACTIVATED);
    }

    public boolean isAuthorizedToWritePost() {
        return isSuperAdmin() || isAuthor() || isAdmin();
    }

    public boolean isBanned() {
        return accountStatus.equals(AccountStatus.BANNED);
    }

    public boolean isPending() {
        return accountStatus.equals(AccountStatus.PENDING);
    }

    public boolean isNew() {
        return id == 0;
    }

    public boolean isAdmin() {
        return role.getTitle().equals("ADMIN");
    }

    public boolean isSuperAdmin() {
        return role.getTitle().equals("SUPER_ADMIN");
    }

    public boolean isAuthor() {
        return role.getTitle().equals("AUTHOR");
    }

    public boolean isSubscriber() {
        return role.getTitle().equals("SUBSCRIBER");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        if (id != user.id) return false;
        if (accountStatus != user.accountStatus) return false;
        if (address != null ? !address.equals(user.address) : user.address != null) return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (firstName != null ? !firstName.equals(user.firstName) : user.firstName != null) return false;
        if (lastName != null ? !lastName.equals(user.lastName) : user.lastName != null) return false;
        if (password != null ? !password.equals(user.password) : user.password != null) return false;
        if (role != null ? !role.equals(user.role) : user.role != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (accountStatus != null ? accountStatus.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", accountStatus=" + accountStatus +
                ", address='" + address + '\'' +
                ", role=" + role +
                '}';
    }
}
