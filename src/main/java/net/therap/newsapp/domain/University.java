package net.therap.newsapp.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.Date;

/**
 * @author sahariar.alam
 * @since 10/1/19
 */
@Entity
@Table(name = "university")
@NamedQueries({
        @NamedQuery(name = "University.findAll", query = "FROM University"),

        @NamedQuery(name = "University.findByTitle", query = "FROM University WHERE title = :title")
})
public class University extends Persistent {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(updatable = false, nullable = false)
    @NotNull(message = "{app.not.empty}")
    @Size(min = 1, max = 100, message = "{app.university.title}")
    private String title;

    @Column(updatable = false, nullable = false)
    @NotNull(message = "{app.not.empty}")
    @Size(min = 5, max = 1000, message = "{app.university.address}")
    private String address;

    @Column(name = "website_url", nullable = false)
    @NotNull(message = "{app.not.empty}")
    @Size(min = 5, max = 300, message = "{app.university.url}")
    private String url;

    @Column(name = "foundation_year", updatable = false, nullable = false)
    @NotNull(message = "{app.notNull.foundation}")
    @Temporal(TemporalType.TIMESTAMP)
    private Date foundation;

    @Column(name = "contact_number", nullable = false)
    @NotNull(message = "{app.not.empty}")
    @Pattern(regexp = "\\d+", message = "{app.invalid.number}")
    @Size(max = 13, message = "{app.university.number}")
    private String number;

    @Column(nullable = false)
    @NotNull(message = "{app.not.empty}")
    private int lat;

    @NotNull(message = "{app.not.empty}")
    private int lon;

    @ManyToOne
    @JoinColumn(name = "created_by")
    private User createdBy;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Date getFoundation() {
        return foundation;
    }

    public void setFoundation(Date foundation) {
        this.foundation = foundation;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public int getLat() {
        return lat;
    }

    public void setLat(int lat) {
        this.lat = lat;
    }

    public int getLon() {
        return lon;
    }

    public void setLon(int lon) {
        this.lon = lon;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public boolean isNew() {
        return id == 0;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof University)) return false;

        University that = (University) o;

        if (id != that.id) return false;
        if (lat != that.lat) return false;
        if (lon != that.lon) return false;
        if (address != null ? !address.equals(that.address) : that.address != null) return false;
        if (createdBy != null ? !createdBy.equals(that.createdBy) : that.createdBy != null) return false;
        if (foundation != null ? !foundation.equals(that.foundation) : that.foundation != null) return false;
        if (number != null ? !number.equals(that.number) : that.number != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (foundation != null ? foundation.hashCode() : 0);
        result = 31 * result + (number != null ? number.hashCode() : 0);
        result = 31 * result + lat;
        result = 31 * result + lon;
        result = 31 * result + (createdBy != null ? createdBy.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "University{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", address='" + address + '\'' +
                ", url='" + url + '\'' +
                ", foundation=" + foundation +
                ", number='" + number + '\'' +
                ", lat=" + lat +
                ", lon=" + lon +
                '}';
    }
}
