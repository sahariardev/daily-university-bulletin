package net.therap.newsapp.helper;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author sahariar.alam
 * @since 9/30/19
 */
@Component
public class PasswordEncoderHelper {

    @Value("${app.password.salt}")
    private static String salt;

    public String hash(String password) {
        return DigestUtils.md5Hex(password + salt);
    }
}
