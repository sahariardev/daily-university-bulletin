package net.therap.newsapp.cmd;

import net.therap.newsapp.domain.News;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author sahariar.alam
 * @since 10/16/19
 */
public class NewsListCmd implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<News> news;

    private long pageCount;

    public NewsListCmd() {
        this.news = new ArrayList<>();
    }

    public List<News> getNews() {
        return news;
    }

    public void setNews(List<News> news) {
        this.news = news;
    }

    public long getPageCount() {
        return pageCount;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }

    @Override
    public String toString() {
        return "NewsListCmd{" +
                "news=" + news +
                ", pageCount=" + pageCount +
                '}';
    }
}
