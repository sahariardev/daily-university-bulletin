package net.therap.newsapp.cmd;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author sahariar.alam
 * @since 10/13/19
 */
public class ProfileCmd implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "{app.canNotBeEmpty}")
    @Size(min = 1, max = 100, message = "{app.firstNameLength}")
    private String firstName;

    @NotNull(message = "{app.canNotBeEmpty}")
    @Size(min = 1, max = 100, message = "{app.lastNameLength}")
    private String lastName;

    @Size(max = 1000)
    private String address;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "ProfileCmd{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
