package net.therap.newsapp.cmd;

import net.therap.newsapp.constant.AccountStatus;
import net.therap.newsapp.domain.Role;

import java.io.Serializable;

/**
 * @author sahariar.alam
 * @since 10/14/19
 */
public class UserListFilterCmd implements Serializable {

    private static final long serialVersionUID = 1L;

    private AccountStatus accountStatus;

    private Role role;

    public AccountStatus getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(AccountStatus accountStatus) {
        this.accountStatus = accountStatus;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "UserListFilterCmd{" +
                "accountStatus=" + accountStatus +
                ", role=" + role +
                '}';
    }
}
