package net.therap.newsapp.cmd;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author sahariar.alam
 * @since 9/30/19
 */
public class LoginCmd implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "{app.not.empty}")
    private String email;

    @NotNull(message = "{app.not.empty}")
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "LoginCmd{" +
                "email='" + email + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
