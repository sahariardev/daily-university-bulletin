package net.therap.newsapp.cmd;

import java.io.Serializable;

/**
 * @author sahariar.alam
 * @since 10/14/19
 */
public class AdminMenuItemCmd implements Serializable {

    private static final long serialVersionUID = 1L;

    private String displayName;

    private String url;

    public AdminMenuItemCmd(String displayName, String url) {
        this.displayName = displayName;
        this.url = url;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return String.format("AdminMenuItemCmd{" +
                "displayName='%s'" +
                ", url='%s'" +
                "}", displayName, url);
    }
}
