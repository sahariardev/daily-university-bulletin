package net.therap.newsapp.cmd;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author sahariar.alam
 * @since 10/3/19
 */
public class PasswordCmd implements Serializable {

    private static final long serialVersionUID = 1L;

    @NotNull(message = "{app.not.empty}")
    private String email;

    @NotNull(message = "{app.not.empty}")
    private String currentPassword;

    @Size(min = 4, message = "{app.password.length.message}")
    private String newPassword;

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "PasswordCmd{" +
                "currentPassword='" + currentPassword + '\'' +
                ", newPassword='" + newPassword + '\'' +
                '}';
    }
}
