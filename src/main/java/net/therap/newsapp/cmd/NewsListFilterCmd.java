package net.therap.newsapp.cmd;

import net.therap.newsapp.constant.PostStatus;

import java.io.Serializable;

import static net.therap.newsapp.constant.ApplicationConstant.FIRST_PAGE;
import static net.therap.newsapp.constant.ApplicationConstant.LIMIT_PER_PAGE;

/**
 * @author sahariar.alam
 * @since 10/7/19
 */
public class NewsListFilterCmd implements Serializable {

    private static final long serialVersionUID = 1L;

    private int page;

    private int limit;

    private PostStatus postStatus;

    public NewsListFilterCmd() {
        this.page = FIRST_PAGE;
        this.limit = LIMIT_PER_PAGE;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public PostStatus getPostStatus() {
        return postStatus;
    }

    public void setPostStatus(PostStatus postStatus) {
        this.postStatus = postStatus;
    }

    @Override
    public String toString() {
        return "NewsListFilterCmd{" +
                "page=" + page +
                ", limit=" + limit +
                ", postStatus=" + postStatus +
                '}';
    }
}
