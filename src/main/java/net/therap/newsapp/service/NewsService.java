package net.therap.newsapp.service;

import net.therap.newsapp.cmd.NewsListCmd;
import net.therap.newsapp.constant.PostStatus;
import net.therap.newsapp.dao.NewsDao;
import net.therap.newsapp.domain.News;
import net.therap.newsapp.domain.University;
import net.therap.newsapp.domain.User;
import net.therap.newsapp.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;

/**
 * @author sahariar.alam
 * @since 10/2/19
 */
@Service
public class NewsService {

    private static final int EMPTY_PAGE_SIZE = 0;

    @Autowired
    private NewsDao newsDao;

    @Autowired
    private UserService userService;

    public News save(News news, String userEmail) {
        User user = userService.findByEmail(userEmail);
        University university = user.getUniversity();
        if (news.isPublished()) {
            news.setPublishedBy(user);
        }
        if (news.isNew()) {
            news.setCreatedBy(user);
            news.setUniversity(university);
        }
        return newsDao.save(news);
    }

    public List<News> findTopThreeNews() {
        Date date = new Date(Calendar.getInstance().getTime().getTime());
        return newsDao.findTop(PostStatus.PUBLISHED, date, 3);
    }

    public List<News> findPublishedNewsByUniversity(University university) {
        Date date = new Date(Calendar.getInstance().getTime().getTime());
        return newsDao.findBy(university, PostStatus.PUBLISHED, date);
    }

    public List<News> search(String keyword) {
        Date date = new Date(Calendar.getInstance().getTime().getTime());
        return newsDao.searchBy(keyword, date, PostStatus.PUBLISHED);
    }

    public News update(News news, String userEmail, long id) {
        User user = userService.findByEmail(userEmail);
        News oldVersion = findBy(id);
        news.setUniversity(oldVersion.getUniversity());
        if (Objects.isNull(news.getImage())) {
            news.setImage(oldVersion.getImage());
        }
        if (news.isPublished()) {
            news.setPublishedBy(user);
        }
        return newsDao.save(news);
    }

    public void save(News news) {
        newsDao.save(news);
    }

    public NewsListCmd findAll(String loggedInUserEmail, int pageSize, int pageNumber, PostStatus postStatus) {
        User user = userService.findByEmail(loggedInUserEmail);
        if (user.isSuperAdmin()) {
            return findNewsForSuperAdmin(postStatus, pageNumber, pageSize);
        } else if (user.isAdmin()) {
            return findNewsForAdmin(postStatus, pageNumber, pageSize, user);
        } else if (user.isAuthor()) {
            return findNewsForAuthor(postStatus, pageNumber, pageSize, user);
        }
        return new NewsListCmd();
    }

    public News findBy(long id) {
        return newsDao.findBy(id).orElseThrow(() -> new NotFoundException("News Not Found"));
    }

    public boolean checkNewsTitleAlreadyExist(News news) {
        return newsDao.findBy(news.getTitle(), news.getId()).isPresent();
    }

    public boolean isAuthorizedUser(String email, long newsId) {
        User user = userService.findByEmail(email);
        News news = findBy(newsId);
        if (user.isAuthor() && news.getCreatedBy().equals(user)) {
            return true;
        } else if (user.isAdmin()
                && news.getCreatedBy().getUniversity().equals(user.getUniversity())) {
            return true;
        } else if (user.isSuperAdmin()) {
            return true;
        }
        return false;
    }

    private NewsListCmd findNewsForSuperAdmin(PostStatus postStatus, int pageNumber, int pageSize) {
        NewsListCmd newsListCmd = new NewsListCmd();
        if (Objects.isNull(postStatus)) {
            newsListCmd.setNews(newsDao.findAll(pageNumber, pageSize));
            newsListCmd.setPageCount(newsDao.findAll(pageNumber, EMPTY_PAGE_SIZE).size());

        } else {
            newsListCmd.setNews(newsDao.findBy(postStatus, pageNumber, pageSize));
            newsListCmd.setPageCount(newsDao.findBy(postStatus, pageNumber, EMPTY_PAGE_SIZE).size());
        }
        return newsListCmd;
    }

    private NewsListCmd findNewsForAdmin(PostStatus postStatus, int pageNumber, int pageSize, User user) {
        NewsListCmd newsListCmd = new NewsListCmd();
        if (Objects.isNull(postStatus)) {
            newsListCmd.setNews(newsDao.findBy(user.getUniversity(), pageNumber, pageSize));
            newsListCmd.setPageCount(newsDao.findBy(user.getUniversity(), pageNumber, EMPTY_PAGE_SIZE).size());
        } else {
            newsListCmd.setNews(newsDao.findBy(user.getUniversity(), postStatus, pageNumber, pageSize));
            newsListCmd.setPageCount(newsDao.findBy(user.getUniversity(), postStatus, pageNumber, EMPTY_PAGE_SIZE).size());
        }
        return newsListCmd;
    }

    private NewsListCmd findNewsForAuthor(PostStatus postStatus, int pageNumber, int pageSize, User user) {
        NewsListCmd newsListCmd = new NewsListCmd();
        if (Objects.isNull(postStatus)) {
            newsListCmd.setNews(newsDao.findBy(user, pageNumber, pageSize));
            newsListCmd.setPageCount(newsDao.findBy(user, pageNumber, EMPTY_PAGE_SIZE).size());
        } else {
            newsListCmd.setNews(newsDao.findBy(user, postStatus, pageNumber, pageSize));
            newsListCmd.setPageCount(newsDao.findBy(user, postStatus, pageNumber, EMPTY_PAGE_SIZE).size());
        }
        return newsListCmd;
    }

}
