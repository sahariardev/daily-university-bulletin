package net.therap.newsapp.service;

import net.therap.newsapp.cmd.ProfileCmd;
import net.therap.newsapp.constant.AccountStatus;
import net.therap.newsapp.constant.RoleType;
import net.therap.newsapp.dao.RoleDao;
import net.therap.newsapp.dao.UserDao;
import net.therap.newsapp.domain.Role;
import net.therap.newsapp.domain.User;
import net.therap.newsapp.exception.NotFoundException;
import net.therap.newsapp.helper.PasswordEncoderHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author sahariar.alam
 * @since 9/30/19
 */
@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private PasswordEncoderHelper passwordEncoderHelper;

    @Autowired
    private RoleDao roleDao;

    public void saveNewUser(User user) {
        user.setPassword(passwordEncoderHelper.hash(user.getPassword()));
        user.setRole(roleDao.findBy(RoleType.SUBSCRIBER.name())
                .orElseThrow(() -> new NotFoundException("Role does not exist")));
        user.setAccountStatus(AccountStatus.PENDING);
        userDao.save(user);
    }

    public void updateUser(ProfileCmd profile, String email) {
        User oldInfo = findByEmail(email);
        oldInfo.setFirstName(profile.getFirstName());
        oldInfo.setLastName(profile.getLastName());
        oldInfo.setAddress(profile.getAddress());
        userDao.save(oldInfo);
    }

    public User findByEmail(String email) {
        return userDao.findBy(email).orElseThrow(() -> new NotFoundException("User Not Found"));
    }

    public void changePassword(String email, String oldPassword, String newPassword) {
        User user = userDao.findBy(email).orElseThrow(() -> new NotFoundException("User Not Found"));
        if (user.getPassword().equals(passwordEncoderHelper.hash(oldPassword))) {
            user.setPassword(passwordEncoderHelper.hash(newPassword));
            userDao.save(user);
        }
    }

    public boolean isValidUser(String email, String password) {
        Optional<User> user = userDao.findBy(email);
        if (user.isPresent()) {
            String encodedPassword = passwordEncoderHelper.hash(password);
            if (user.get().getPassword().equals(encodedPassword)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkEmailExist(String email) {
        return userDao.findBy(email).isPresent();
    }

    public void updateUserRoleAndStatus(User user) {
        Role role = roleDao.findBy(user.getRole().getTitle())
                .orElseThrow(() -> new NotFoundException("Role does not exist"));
        AccountStatus status = user.getAccountStatus();
        user = findByEmail(user.getEmail());
        user.setRole(role);
        user.setAccountStatus(status);
        userDao.save(user);
    }

    public List<User> findAll(String userRole, AccountStatus status) {
        Role role = null;
        if (Objects.nonNull(userRole)) {
            role = roleDao.findBy(userRole)
                    .orElseThrow(() -> new NotFoundException("Role does not exist"));
        }
        return userDao.findAll(role, status);
    }
}
