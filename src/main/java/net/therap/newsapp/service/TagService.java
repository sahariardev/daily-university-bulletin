package net.therap.newsapp.service;

import net.therap.newsapp.dao.TagDao;
import net.therap.newsapp.domain.Tag;
import net.therap.newsapp.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author sahariar.alam
 * @since 10/1/19
 */
@Service
public class TagService {

    @Autowired
    private TagDao tagDao;

    public List<Tag> findAll() {
        return tagDao.findAll();
    }

    public void saveNewTag(Tag tag, User user) {
        tag.setCreatedBy(user);
        tagDao.save(tag);
    }

    public boolean tagTitleExist(String title) {
        return tagDao.findBy(title).isPresent();
    }

    public List<Tag> findAllFromGivenIds(List<Long> tagIds) {
        return tagDao.findAllInList(tagIds);
    }
}
