package net.therap.newsapp.service;

import net.therap.newsapp.dao.UniversityDao;
import net.therap.newsapp.domain.University;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * @author sahariar.alam
 * @since 10/1/19
 */
@Service
public class UniversityService {

    @Autowired
    private UniversityDao universityDao;

    public List<University> findAll() {
        return universityDao.findAll();
    }

    public University findById(long id) {
        return universityDao.findBy(id).get();
    }

    public University findByTitle(String title) {
        return universityDao.findBy(title).get();

    }

    public boolean universityTitleExist(String title) {
        Optional<University> universityOptional = universityDao.findBy(title);
        return universityOptional.isPresent();
    }

    public void save(University university) {
        universityDao.save(university);
    }
}
