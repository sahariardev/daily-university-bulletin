package net.therap.newsapp.dao;

import net.therap.newsapp.domain.University;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author sahariar.alam
 * @since 10/1/19
 */

@Repository
public class UniversityDao extends BaseDao<University> {

    public List<University> findAll() {
        return findAll("University.findAll", University.class);
    }

    public Optional<University> findBy(long id) {
        return findBy(University.class, id);
    }

    public Optional<University> findBy(String title) {
        TypedQuery<University> query = em.createNamedQuery("University.findByTitle", University.class)
                .setParameter("title", title);
        return query.getResultList().size() == 1 ?
                Optional.of(query.getResultList().get(0)) : Optional.<University>empty();
    }

    @Transactional
    public University save(University university) {
        return save(university, university.isNew());
    }
}
