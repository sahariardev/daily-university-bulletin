package net.therap.newsapp.dao;

import net.therap.newsapp.constant.AccountStatus;
import net.therap.newsapp.domain.Role;
import net.therap.newsapp.domain.User;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author sahariar.alam
 * @since 9/30/19
 */
@Repository
public class UserDao extends BaseDao<User> {

    public List<User> findAll() {
        return findAll("User.findAll", User.class);
    }

    public List<User> findAll(Role role, AccountStatus status) {
        if (Objects.nonNull(role)) {
            if (Objects.nonNull(status)) {
                return findBy(role, status);
            }
            return findBy(role);
        }
        if (Objects.nonNull(status)) {
            return findBy(status);
        }
        return findAll();
    }

    public Optional<User> findBy(long id) {
        return findBy(User.class, id);
    }

    public Optional<User> findBy(String email) {
        TypedQuery<User> query = em.createNamedQuery("User.findByEmail", User.class)
                .setParameter("email", email);
        List<User> users = query.getResultList();
        return users.size() == 1 ? Optional.of(users.get(0)) : Optional.<User>empty();
    }

    @Transactional
    public User save(User user) {
        return save(user, user.isNew());
    }

    private List<User> findBy(Role role) {
        return em.createNamedQuery("User.findByRole", User.class)
                .setParameter("role", role)
                .getResultList();
    }

    private List<User> findBy(AccountStatus status) {
        return em.createNamedQuery("User.findByStatus", User.class)
                .setParameter("status", status)
                .getResultList();
    }

    private List<User> findBy(Role role, AccountStatus status) {
        return em.createNamedQuery("User.findByRoleAndStatus", User.class)
                .setParameter("status", status)
                .setParameter("role", role)
                .getResultList();
    }
}
