package net.therap.newsapp.dao;

import net.therap.newsapp.constant.PostStatus;
import net.therap.newsapp.domain.News;
import net.therap.newsapp.domain.University;
import net.therap.newsapp.domain.User;
import net.therap.newsapp.exception.NotFoundException;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author sahariar.alam
 * @since 10/2/19
 */
@Repository
public class NewsDao extends BaseDao<News> {

    public List<News> findAll(int pageNumber, int pageSize) {
        TypedQuery<News> query = em.createNamedQuery("News.findAll", News.class);
        setPagination(query, pageNumber, pageSize);
        return query.getResultList();
    }

    public List<News> searchBy(String keyword, Date date, PostStatus postStatus) {
        return em.createNamedQuery("News.search", News.class).setParameter("keyword", "%" + keyword + "%")
                .setParameter("date", date)
                .setParameter("currentStatus", postStatus)
                .getResultList();
    }

    public List<News> findTop(PostStatus postStatus, Date date, int maxResult) {
        return em.createNamedQuery("News.findTopNews", News.class).setParameter("date", date)
                .setParameter("currentStatus", postStatus)
                .setMaxResults(maxResult)
                .getResultList();
    }

    public List<News> findBy(University university, PostStatus postStatus, Date date) {
        return em.createNamedQuery("News.findPublishedNewsByUniversity", News.class)
                .setParameter("university", university)
                .setParameter("date", date)
                .setParameter("currentStatus", postStatus)
                .getResultList();
    }

    public List<News> findBy(University university, int pageNumber, int pageSize) {
        TypedQuery<News> query = em.createNamedQuery("News.findByUniversity", News.class)
                .setParameter("university", university);
        setPagination(query, pageNumber, pageSize);
        return query.getResultList();
    }

    public List<News> findBy(User user, int pageNumber, int pageSize) {
        TypedQuery<News> query = em.createNamedQuery("News.findByUser", News.class)
                .setParameter("user", user);
        setPagination(query, pageNumber, pageSize);
        return query.getResultList();
    }

    public Optional<News> findBy(long id) {
        return findBy(News.class, id);
    }

    public List<News> findBy(PostStatus postStatus, int pageNumber, int pageSize) {
        TypedQuery<News> query = em.createNamedQuery("News.findByStatus", News.class)
                .setParameter("currentStatus", postStatus);
        setPagination(query, pageNumber, pageSize);
        return query.getResultList();
    }

    public Optional<News> findBy(String title, long id) {
        TypedQuery<News> query = em.createNamedQuery("News.findByTitleAndNotEqualId", News.class)
                .setParameter("title", title)
                .setParameter("id", id);
        return query.getResultList().size() != 0 ? Optional.of(query.getSingleResult()) : Optional.<News>empty();
    }

    public List<News> findBy(University university, PostStatus postStatus, int pageNumber, int pageSize) {
        TypedQuery<News> query = em.createNamedQuery("News.findByUniversityAndStatus", News.class)
                .setParameter("currentStatus", postStatus)
                .setParameter("university", university);
        setPagination(query, pageNumber, pageSize);
        return query.getResultList();
    }

    public List<News> findBy(User user, PostStatus postStatus, int pageNumber, int pageSize) {
        TypedQuery<News> query = em.createNamedQuery("News.findByUserAndStatus", News.class)
                .setParameter("user", user)
                .setParameter("currentStatus", postStatus);
        setPagination(query, pageNumber, pageSize);
        return query.getResultList();
    }

    public Optional<News> findBy(String title) {
        TypedQuery<News> query = em.createNamedQuery("News.findByTitle", News.class)
                .setParameter("title", title);
        if (query.getResultList().size() != 1) {
            throw new NotFoundException("Not found");
        }
        return Optional.of(query.getSingleResult());
    }

    @Transactional
    public News save(News news) {
        return save(news, news.isNew());
    }

    private void setPagination(TypedQuery<News> query, int pageNumber, int pageSize) {
        if (pageSize > 0) {
            query.setFirstResult((pageNumber - 1) * pageSize);
            query.setMaxResults(pageSize);
        }
    }
}
