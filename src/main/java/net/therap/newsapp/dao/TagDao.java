package net.therap.newsapp.dao;

import net.therap.newsapp.domain.Tag;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author sahariar.alam
 * @since 10/1/19
 */
@Repository
public class TagDao extends BaseDao<Tag> {

    public List<Tag> findAll() {
        return findAll("Tag.findAll", Tag.class);
    }

    public List<Tag> findAllInList(List<Long> tagIds) {
        return em.createNamedQuery("Tag.findByIds", Tag.class)
                .setParameter("ids", tagIds)
                .getResultList();
    }

    public Optional<Tag> findBy(String title) {
        TypedQuery<Tag> query = em.createNamedQuery("Tag.findByTitle", Tag.class)
                .setParameter("title", title);
        return query.getResultList().size() == 1 ? Optional.of(query.getResultList().get(0)) : Optional.<Tag>empty();
    }

    @Transactional
    public Tag save(Tag tag) {
        return save(tag, tag.isNew());
    }
}
