package net.therap.newsapp.dao;

import net.therap.newsapp.exception.NotFoundException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author sahariar.alam
 * @since 10/16/19
 */
@Repository
public abstract class BaseDao<T> {

    @PersistenceContext
    protected EntityManager em;

    public Optional<T> findBy(Class<T> entityClass, long id) {
        T t = em.find(entityClass, id);
        if (Objects.isNull(t)) {
            throw new NotFoundException("Not Found");
        }
        return Optional.ofNullable(t);
    }

    public List<T> findAll(String query, Class<T> entityClass) {
        return em.createNamedQuery(query, entityClass).getResultList();
    }

    public T save(T t, boolean isNew) {
        if (isNew) {
            em.persist(t);
        } else {
            t = em.merge(t);
        }
        return t;
    }
}
