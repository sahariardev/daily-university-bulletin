package net.therap.newsapp.dao;

import net.therap.newsapp.domain.Role;
import net.therap.newsapp.exception.NotFoundException;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

/**
 * @author sahariar.alam
 * @since 9/29/19
 */
@Repository
public class RoleDao extends BaseDao<Role> {

    public Optional<Role> findBy(String title) {
        TypedQuery<Role> query = em.createNamedQuery("Role.findByTitle", Role.class)
                .setParameter("title", title);
        List<Role> roles = query.getResultList();
        if (roles.size() != 1) {
            throw new NotFoundException("Role not found");
        }
        return Optional.of(roles.get(0));
    }

    @Transactional
    public Role save(Role role) {
        return save(role, role.isNew());
    }
}
