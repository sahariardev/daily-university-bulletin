package net.therap.newsapp.filter;

import net.therap.newsapp.constant.AccountStatus;
import net.therap.newsapp.constant.ApplicationConstant;
import net.therap.newsapp.domain.User;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

import static net.therap.newsapp.constant.URL.*;

/**
 * @author sahariar.alam
 * @since 9/11/19
 */
@Component
public class AuthenticationFilter implements Filter {

    private String urlPrefix;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.urlPrefix = filterConfig.getInitParameter("prefix");
    }

    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse res = (HttpServletResponse) servletResponse;
        servletResponse(req, res);
        filterChain.doFilter(req, res);
    }

    @Override
    public void destroy() {

    }

    private void servletResponse(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        HttpSession session = req.getSession();
        res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        res.setHeader("Pragma", "no-cache");
        res.setDateHeader("Expires", 0);
        User user = (User) session.getAttribute(ApplicationConstant.USER);

        if (req.getRequestURI().startsWith(urlPrefix)) {
            if (Objects.isNull(user)) {
                res.sendRedirect(req.getContextPath() + AUTH + LOGIN);
            } else {
                checkActivatedUser(req, res, user);
                checkNewsAccess(req, res, user);
                checkTagAccess(req, res, user);
                checkUniversityAccess(req, res, user);
            }
        }
    }

    private void checkActivatedUser(HttpServletRequest req, HttpServletResponse res, User user)
            throws IOException, ServletException {
        if (!req.getRequestURI().startsWith(USER)
                && !user.getAccountStatus().equals(AccountStatus.ACTIVATED)) {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    private void checkNewsAccess(HttpServletRequest req, HttpServletResponse res, User user)
            throws IOException, ServletException {
        if (req.getRequestURI().startsWith(NEWS)) {
            if (!(user.isAuthor() || user.isSuperAdmin() || user.isAdmin())
                    && !req.getRequestURI().contains("comment")) {
                res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            }
        }
    }

    private void checkTagAccess(HttpServletRequest req, HttpServletResponse res, User user)
            throws IOException, ServletException {
        if (req.getRequestURI().startsWith(TAGS) && !(user.isAdmin() || user.isSuperAdmin())) {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    private void checkUniversityAccess(HttpServletRequest req, HttpServletResponse res, User user)
            throws IOException, ServletException {
        if (req.getRequestURI().startsWith(UNIVERSITY)
                && !user.isSuperAdmin()) {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
    }
}
