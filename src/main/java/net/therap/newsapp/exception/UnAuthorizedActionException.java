package net.therap.newsapp.exception;

/**
 * @author sahariar.alam
 * @since 10/1/19
 */
public class UnAuthorizedActionException extends RuntimeException {
    public UnAuthorizedActionException(String message) {
        super(message);
    }
}
