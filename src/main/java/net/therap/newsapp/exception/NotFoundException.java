package net.therap.newsapp.exception;

/**
 * @author sahariar.alam
 * @since 10/1/19
 */
public class NotFoundException extends RuntimeException {
    public NotFoundException(String message) {
        super(message);
    }
}
