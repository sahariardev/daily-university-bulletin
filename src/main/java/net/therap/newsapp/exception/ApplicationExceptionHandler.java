package net.therap.newsapp.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author sahariar.alam
 * @since 10/6/19
 */
@ControllerAdvice
public class ApplicationExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    public String handleNotFoundException() {
        return "404";
    }

    @ExceptionHandler(UnAuthorizedActionException.class)
    public String handleUnauthorizedException() {
        return "403";
    }
}
