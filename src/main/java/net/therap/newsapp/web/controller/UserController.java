package net.therap.newsapp.web.controller;

import net.therap.newsapp.cmd.UserListFilterCmd;
import net.therap.newsapp.constant.AccountStatus;
import net.therap.newsapp.constant.ApplicationConstant;
import net.therap.newsapp.constant.RoleType;
import net.therap.newsapp.domain.User;
import net.therap.newsapp.exception.UnAuthorizedActionException;
import net.therap.newsapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Objects;

import static net.therap.newsapp.constant.URL.CHANGE_USER_ROLE_STATUS;
import static net.therap.newsapp.constant.URL.USER;

/**
 * @author sahariar.alam
 * @since 9/30/19
 */
@Controller
@RequestMapping(USER)
public class UserController {

    private static final String USER_LIST = "userList";

    private static final String USER_LIST_RESULT = "userListResult";

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSourceAccessor msa;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getAllUsers(HttpSession session,
                              Model model) {
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        checkActivatedSuperAdmin(userService.findByEmail(user.getEmail()));
        model.addAttribute("userCmd", new UserListFilterCmd());
        model.addAttribute("status", AccountStatus.values());
        model.addAttribute("roles", RoleType.values());
        return USER_LIST;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String getAllUsers(@ModelAttribute("userCmd") UserListFilterCmd userCmd,
                              HttpSession session,
                              Model model) {
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        checkActivatedSuperAdmin(userService.findByEmail(user.getEmail()));
        List<User> users = userService
                .findAll(Objects.nonNull(userCmd.getRole()) && userCmd.getRole().getTitle() != null
                        ? userCmd.getRole().getTitle() : null, userCmd.getAccountStatus());
        model.addAttribute("users", users);
        model.addAttribute("userModel", new User());
        model.addAttribute("status", AccountStatus.values());
        model.addAttribute("roles", RoleType.values());
        return USER_LIST_RESULT;
    }

    @RequestMapping(value = CHANGE_USER_ROLE_STATUS, method = RequestMethod.POST)
    public String getAllUsers(@ModelAttribute User userModel,
                              HttpSession session,
                              Model model) {
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        checkActivatedSuperAdmin(userService.findByEmail(user.getEmail()));
        userService.updateUserRoleAndStatus(userModel);
        List<User> users = userService.findAll(null, null);
        model.addAttribute("message", msa.getMessage("app.successfully.updated"));
        model.addAttribute("users", users);
        model.addAttribute("userModel", new User());
        model.addAttribute("status", AccountStatus.values());
        model.addAttribute("roles", RoleType.values());
        return USER_LIST_RESULT;
    }

    private void checkActivatedSuperAdmin(User user) {
        if (!user.isSuperAdmin() || !user.isActivated()) {
            throw new UnAuthorizedActionException(msa.getMessage("app.unauthorized.action"));
        }
    }
}
