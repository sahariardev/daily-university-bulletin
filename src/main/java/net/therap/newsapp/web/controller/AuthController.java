package net.therap.newsapp.web.controller;

import net.therap.newsapp.cmd.LoginCmd;
import net.therap.newsapp.constant.ApplicationConstant;
import net.therap.newsapp.domain.User;
import net.therap.newsapp.service.UniversityService;
import net.therap.newsapp.service.UserService;
import net.therap.newsapp.web.validator.LoginCmdValidator;
import net.therap.newsapp.web.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static net.therap.newsapp.constant.URL.*;

/**
 * @author sahariar.alam
 * @since 9/30/19
 */
@Controller
@RequestMapping(AUTH)
public class AuthController {

    private static final String LOGIN_VIEW = "loginForm";

    private static final String SIGN_UP_FORM_VIEW = "userForm";

    private static final int SESSION_TIME = 30 * 60;

    @Autowired
    private LoginCmdValidator loginValidator;

    @Autowired
    private UserService userService;

    @Autowired
    private UniversityService universityService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private MessageSourceAccessor msa;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @RequestMapping(value = LOGIN, method = RequestMethod.GET)
    public String login(Model model) {
        model.addAttribute("login", new LoginCmd());
        return LOGIN_VIEW;
    }

    @RequestMapping(value = LOGIN, method = RequestMethod.POST)
    public String login(@Valid @ModelAttribute("login") LoginCmd login,
                        Errors errors,
                        SessionStatus status,
                        HttpSession session,
                        Model model) {
        loginValidator.validate(login, errors);
        if (errors.hasErrors()) {
            return LOGIN_VIEW;
        }
        User user = userService.findByEmail(login.getEmail());
        login.setPassword(null);
        user.setPassword(null);
        if (user.isPending()) {
            model.addAttribute("message", msa.getMessage("app.account.not.activated"));
            return LOGIN_VIEW;
        }
        if (user.isBanned()) {
            model.addAttribute("message", msa.getMessage("app.account.banned"));
            return LOGIN_VIEW;
        }
        session.setMaxInactiveInterval(SESSION_TIME);
        session.setAttribute(ApplicationConstant.USER, user);
        status.setComplete();
        return REDIRECT + HOME;
    }

    @RequestMapping(value = LOGOUT, method = RequestMethod.POST)
    public String logout(HttpSession session, SessionStatus status, RedirectAttributes redirectAttributes) {
        status.setComplete();
        session.invalidate();
        redirectAttributes.addFlashAttribute("message", msa.getMessage("app.loggedOut"));
        return REDIRECT + AUTH + LOGIN;
    }

    @RequestMapping(value = SIGN_UP_FORM, method = RequestMethod.GET)
    public String createNewUser(Model model) {
        model.addAttribute(new User());
        model.addAttribute("universities", universityService.findAll());
        return SIGN_UP_FORM_VIEW;
    }

    @RequestMapping(value = SIGN_UP_FORM, method = RequestMethod.POST)
    public String saveUser(@Valid @ModelAttribute User user, Errors errors, Model model) {
        userValidator.validate(user, errors);
        if (errors.hasErrors()) {
            model.addAttribute("universities", universityService.findAll());
            return SIGN_UP_FORM_VIEW;
        }
        userService.saveNewUser(user);
        return REDIRECT + AUTH + LOGIN;
    }
}
