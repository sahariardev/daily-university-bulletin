package net.therap.newsapp.web.controller;

import net.therap.newsapp.cmd.NewsListCmd;
import net.therap.newsapp.cmd.NewsListFilterCmd;
import net.therap.newsapp.constant.ApplicationConstant;
import net.therap.newsapp.constant.PostStatus;
import net.therap.newsapp.domain.Comment;
import net.therap.newsapp.domain.News;
import net.therap.newsapp.domain.User;
import net.therap.newsapp.exception.NotFoundException;
import net.therap.newsapp.exception.UnAuthorizedActionException;
import net.therap.newsapp.service.NewsService;
import net.therap.newsapp.service.TagService;
import net.therap.newsapp.service.UserService;
import net.therap.newsapp.web.editor.DateEditor;
import net.therap.newsapp.web.editor.TagEditor;
import net.therap.newsapp.web.validator.NewsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static net.therap.newsapp.constant.URL.*;

/**
 * @author sahariar.alam
 * @since 10/2/19
 */
@Controller
@RequestMapping(NEWS)
public class NewsController {

    private static final String NEWS_FORM_VIEW = "newsForm";

    private static final String NEWS_LIST_VIEW = "newsList";

    private static final String NEWS_LIST_RESULT_VIEW = "newsListResult";

    @Autowired
    private NewsService newsService;

    @Autowired
    private UserService userService;

    @Autowired
    private TagService tagService;

    @Autowired
    private TagEditor tagEditor;

    @Autowired
    private NewsValidator newsValidator;

    @Autowired
    private DateEditor dateEditor;

    @Autowired
    private MessageSourceAccessor msa;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.registerCustomEditor(Date.class, dateEditor);
        binder.registerCustomEditor(List.class, "tags", tagEditor);
    }

    @RequestMapping(NEWS_CREATE)
    public String create(Model model, HttpSession session) {
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        setPostStatus(model, user);
        model.addAttribute("news", new News());
        model.addAttribute("allTags", tagService.findAll());
        return NEWS_FORM_VIEW;
    }

    @RequestMapping(value = NEWS_CREATE, method = RequestMethod.POST)
    public String create(@Valid @ModelAttribute News news,
                         Errors errors,
                         RedirectAttributes redirectAttributes,
                         HttpSession session,
                         Model model) throws IOException {
        String message = msa.getMessage("app.successfully.saved");
        return saveUpdate(news, errors, model, redirectAttributes, session, message,
                user -> checkSaveNewsAuthorization(userService.findByEmail(user.getEmail()), news),
                user -> newsService.save(news, user.getEmail()));
    }

    @RequestMapping(value = UPDATE, method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute News news,
                         Errors errors, Model model,
                         @PathVariable long id,
                         RedirectAttributes redirectAttributes,
                         HttpSession session) throws IOException {
        String message = msa.getMessage("app.successfully.updated");
        return saveUpdate(news, errors, model, redirectAttributes, session, message,
                user -> checkUpdateNewsAuthorization(userService.findByEmail(user.getEmail()),
                        news, newsService.findBy(news.getId())),
                user -> newsService.update(news, user.getEmail(), id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public String findAll(Model model) {
        model.addAttribute("status", PostStatus.values());
        model.addAttribute("newsFilter", new NewsListFilterCmd());
        return NEWS_LIST_VIEW;
    }

    @RequestMapping(method = RequestMethod.POST)
    public String findAll(@ModelAttribute("newsFilter") NewsListFilterCmd newsListFilterCmd,
                          HttpSession session,
                          Model model) {
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        NewsListCmd newsListCmd = newsService.findAll(user.getEmail(), newsListFilterCmd.getLimit(),
                newsListFilterCmd.getPage(), newsListFilterCmd.getPostStatus());
        model.addAttribute("status", PostStatus.values());
        model.addAttribute("news", newsListCmd.getNews());
        model.addAttribute("totalNews", newsListCmd.getPageCount());
        return NEWS_LIST_RESULT_VIEW;

    }

    @RequestMapping(value = UPDATE, method = RequestMethod.GET)
    public String update(@PathVariable long id,
                         HttpSession session,
                         Model model) {
        News news = newsService.findBy(id);
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        if (!newsService.isAuthorizedUser(user.getEmail(), id)) {
            throw new NotFoundException("Not Found");
        }
        setPostStatus(model, user);
        model.addAttribute("news", news);
        model.addAttribute("allTags", tagService.findAll());
        model.addAttribute("comment", new Comment());
        return NEWS_FORM_VIEW;
    }

    @RequestMapping(value = DELETE_COMMENT, method = RequestMethod.POST)
    public String deleteComment(@ModelAttribute Comment comment,
                                @PathVariable long id,
                                RedirectAttributes redirectAttributes,
                                HttpSession session) {
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        checkRemoveCommentAuthorization(userService.findByEmail(user.getEmail()));
        News news = newsService.findBy(id);
        news.removeComment(comment);
        newsService.save(news);
        redirectAttributes.addFlashAttribute("message", msa.getMessage("app.comment.successfully.deleted"));
        return REDIRECT + NEWS + EDIT + id;
    }

    @RequestMapping(value = SAVE_COMMENT, method = RequestMethod.POST)
    public String addComment(@Valid @ModelAttribute Comment comment,
                             Errors errors,
                             @PathVariable long id,
                             RedirectAttributes redirectAttributes,
                             HttpSession session) {
        if (errors.hasErrors()) {
            redirectAttributes.addFlashAttribute("error", errors.getFieldError("body").getDefaultMessage());
            return REDIRECT + NEWS_URL + id;
        }
        User user = userService.findByEmail(((User) session.getAttribute(ApplicationConstant.USER)).getEmail());
        News news = newsService.findBy(id);
        if (user.isBanned()) {
            throw new UnAuthorizedActionException(msa.getMessage("app.unauthorized.action"));
        }
        comment.setCreatedBy(user);
        news.addNewComment(comment);
        newsService.save(news);
        return REDIRECT + NEWS_URL + id;
    }

    private void setPostStatus(Model model, User user) {
        if (user.isAdmin() || user.isSuperAdmin()) {
            model.addAttribute("availableStatus",
                    Arrays.stream(PostStatus.values())
                            .filter(postStatus -> !postStatus.equals(PostStatus.REQUEST_FOR_PUBLISH))
                            .collect(Collectors.toList()));
        } else {
            model.addAttribute("availableStatus",
                    Arrays.stream(PostStatus.values())
                            .filter(postStatus -> !postStatus.equals(PostStatus.PUBLISHED))
                            .collect(Collectors.toList()));
        }
    }

    private void checkUpdateNewsAuthorization(User user, News news, News oldNews) {
        if ((news.isPublished() && !(user.isAdmin() || user.isSuperAdmin()))
                || !user.isActivated() || !user.isAuthorizedToWritePost()) {
            throw new UnAuthorizedActionException(msa.getMessage("app.unauthorized.action"));
        }
        if (oldNews.isPublished()) {
            throw new UnAuthorizedActionException(msa.getMessage("app.news.has.published"));
        }
        if (user.isAuthor() && !oldNews.getCreatedBy().equals(user)) {
            throw new UnAuthorizedActionException(msa.getMessage("app.unauthorized.action"));
        }
        if (user.isAdmin() && !oldNews.getCreatedBy().getUniversity().equals(user.getUniversity())) {
            throw new UnAuthorizedActionException(msa.getMessage("app.unauthorized.action"));
        }
    }

    private void checkSaveNewsAuthorization(User user, News news) {
        if ((news.isPublished() && !(user.isAdmin() || user.isSuperAdmin()))
                || !user.isActivated() || !user.isAuthorizedToWritePost()) {
            throw new UnAuthorizedActionException(msa.getMessage("app.unauthorized.action"));
        }
    }

    private void checkRemoveCommentAuthorization(User user) {
        if (user.isBanned() || (!(user.isAdmin() || user.isSuperAdmin() || user.isAuthor()))) {
            throw new UnAuthorizedActionException(msa.getMessage("app.unauthorized.action"));
        }
    }

    private String saveUpdate(News news,
                              Errors errors,
                              Model model,
                              RedirectAttributes redirectAttributes,
                              HttpSession session,
                              String message,
                              Consumer<User> check,
                              Function<User, News> save) throws IOException {
        newsValidator.validate(news, errors);
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        if (errors.hasErrors()) {
            setPostStatus(model, user);
            model.addAttribute("allTags", tagService.findAll());
            return NEWS_FORM_VIEW;
        }
        check.accept(user);
        if (!news.getImageFile().getOriginalFilename().isEmpty()) {
            news.setImage(news.getImageFile().getBytes());
        }
        news = save.apply(user);
        redirectAttributes.addFlashAttribute("message", message);
        return REDIRECT + NEWS + EDIT + news.getId();
    }
}
