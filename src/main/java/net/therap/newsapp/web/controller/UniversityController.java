package net.therap.newsapp.web.controller;

import net.therap.newsapp.constant.ApplicationConstant;
import net.therap.newsapp.domain.University;
import net.therap.newsapp.domain.User;
import net.therap.newsapp.exception.UnAuthorizedActionException;
import net.therap.newsapp.service.UniversityService;
import net.therap.newsapp.service.UserService;
import net.therap.newsapp.web.editor.DateEditor;
import net.therap.newsapp.web.validator.UniversityValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.sql.Date;

import static net.therap.newsapp.constant.URL.*;

/**
 * @author sahariar.alam
 * @since 10/1/19
 */

@Controller
@RequestMapping(UNIVERSITY)
public class UniversityController {

    private static final String UNIVERSITY_LIST_VIEW = "universityList";

    private static final String UNIVERSITY_VIEW = "university";

    private static final String UNIVERSITY_FORM = "universityForm";

    private static final String UNIVERSITY_UPDATE_FORM = "universityUpdateForm";

    @Autowired
    private UniversityService universityService;

    @Autowired
    private UserService userService;

    @Autowired
    private UniversityValidator universityValidator;

    @Autowired
    private DateEditor dateEditor;

    @Autowired
    private MessageSourceAccessor msa;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.registerCustomEditor(Date.class, "foundation", dateEditor);
        binder.addValidators(universityValidator);
    }

    @RequestMapping()
    public String getAll(Model model) {
        model.addAttribute("universities", universityService.findAll());
        return UNIVERSITY_LIST_VIEW;
    }

    @RequestMapping(PATH_VARIABLE_ID)
    public String getAll(@PathVariable long id, Model model) {
        model.addAttribute("university", universityService.findById(id));
        return UNIVERSITY_VIEW;
    }

    @RequestMapping(UNIVERSITY_CREATE)
    public String createNew(Model model) {
        model.addAttribute("university", new University());
        return UNIVERSITY_FORM;
    }

    @RequestMapping(value = UNIVERSITY_CREATE, method = RequestMethod.POST)
    public String createNew(@Valid @ModelAttribute University university,
                            Errors errors,
                            HttpSession session,
                            RedirectAttributes redirectAttributes) {
        if (errors.hasErrors()) {
            return UNIVERSITY_FORM;
        }
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        checkUniversitySaveAuthorization(userService.findByEmail(user.getEmail()));
        universityService.save(university);
        redirectAttributes.addFlashAttribute("message", msa.getMessage("app.successfully.saved"));
        return REDIRECT + UNIVERSITY;
    }

    @RequestMapping(UPDATE)
    public String update(@PathVariable long id, Model model) {
        University university = universityService.findById(id);
        model.addAttribute("university", university);
        return UNIVERSITY_UPDATE_FORM;
    }

    @RequestMapping(value = UPDATE, method = RequestMethod.POST)
    public String createNew(@Valid @ModelAttribute University university,
                            Errors errors,
                            @PathVariable long id,
                            HttpSession session,
                            RedirectAttributes redirectAttributes) {
        if (errors.hasErrors()) {
            return UNIVERSITY_UPDATE_FORM;
        }
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        checkUniversitySaveAuthorization(userService.findByEmail(user.getEmail()));
        universityService.save(university);
        redirectAttributes.addFlashAttribute("message", msa.getMessage("app.successfully.updated"));
        return REDIRECT + UNIVERSITY + ROOT + id;
    }

    private void checkUniversitySaveAuthorization(User user) {
        if (!user.isSuperAdmin() || !user.isActivated()) {
            throw new UnAuthorizedActionException(msa.getMessage("app.unauthorized.action"));
        }
    }
}
