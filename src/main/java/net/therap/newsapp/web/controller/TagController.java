package net.therap.newsapp.web.controller;

import net.therap.newsapp.constant.ApplicationConstant;
import net.therap.newsapp.domain.Tag;
import net.therap.newsapp.domain.User;
import net.therap.newsapp.exception.UnAuthorizedActionException;
import net.therap.newsapp.service.TagService;
import net.therap.newsapp.service.UserService;
import net.therap.newsapp.web.validator.TagValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static net.therap.newsapp.constant.URL.*;

/**
 * @author sahariar.alam
 * @since 10/1/19
 */
@Controller
@RequestMapping(TAGS)
public class TagController {

    private static final String TAG_LIST_VIEW = "tagList";

    private static final String TAG_FORM_VIEW = "tagForm";

    @Autowired
    private TagService tagService;

    @Autowired
    private UserService userService;

    @Autowired
    private TagValidator tagExistValidator;

    @Autowired
    private MessageSourceAccessor msa;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
        binder.addValidators(tagExistValidator);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String tags(Model model) {
        model.addAttribute("tags", tagService.findAll());
        return TAG_LIST_VIEW;
    }

    @RequestMapping(value = NEWS_CREATE, method = RequestMethod.GET)
    public String tagForm(Model model) {
        model.addAttribute("tag", new Tag());
        return TAG_FORM_VIEW;
    }

    @RequestMapping(value = NEWS_CREATE, method = RequestMethod.POST)
    public String tagForm(@Valid @ModelAttribute Tag tag,
                          Errors errors,
                          HttpSession session,
                          RedirectAttributes redirectAttributes) {
        if (errors.hasErrors()) {
            return TAG_FORM_VIEW;
        }
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        checkSaveTagAuthorization(userService.findByEmail(user.getEmail()));
        tagService.saveNewTag(tag, user);
        redirectAttributes.addFlashAttribute("message", msa.getMessage("app.added.tag"));
        return REDIRECT + TAGS;
    }

    private void checkSaveTagAuthorization(User user) {
        if (!(user.isAdmin() || user.isSuperAdmin()) || !user.isActivated()) {
            throw new UnAuthorizedActionException(msa.getMessage("app.unauthorized.action"));
        }
    }
}
