package net.therap.newsapp.web.controller;

import net.therap.newsapp.constant.AccountStatus;
import net.therap.newsapp.constant.PostStatus;
import net.therap.newsapp.domain.Comment;
import net.therap.newsapp.domain.News;
import net.therap.newsapp.domain.University;
import net.therap.newsapp.exception.NotFoundException;
import net.therap.newsapp.service.NewsService;
import net.therap.newsapp.service.UniversityService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.util.Calendar;
import java.util.List;

import static net.therap.newsapp.constant.URL.*;

/**
 * @author sahariar.alam
 * @since 9/30/19
 */

@Controller
public class HomeController {

    private static final String HOME_VIEW = "home";

    private static final String NEWS_DETAIL_VIEW = "newsDetail";

    private static final String UNIVERSITY_DETAIL_WITH_NEWS_VIEW = "universityDetailWithNews";

    @Autowired
    private NewsService newsService;

    @Autowired
    private UniversityService universityService;

    @Autowired
    private MessageSourceAccessor msa;

    @RequestMapping(ROOT)
    public String showHome() {
        return REDIRECT + HOME;
    }

    @RequestMapping(HOME)
    public String showHome(Model model) {
        setReferenceData(model);
        return HOME_VIEW;
    }

    @RequestMapping(value = HOME, method = RequestMethod.POST)
    public String showHome(@ModelAttribute News news, Model model) {
        setReferenceData(model);
        if (!news.getTitle().isEmpty()) {
            model.addAttribute("result", newsService.search(news.getTitle()));
        }
        return HOME_VIEW;
    }

    @RequestMapping(COMMON_NEWS_URL)
    public String showNews(@PathVariable long id, Model model) {
        News news = newsService.findBy(id);
        Date currentDate = new Date(Calendar.getInstance().getTimeInMillis());
        if (!(news.getStatus().equals(PostStatus.PUBLISHED)) || news.getPublishDate().after(currentDate)
                || news.getEndDate().before(currentDate)
                || news.getCreatedBy().getAccountStatus().equals(AccountStatus.BANNED)) {
            throw new NotFoundException(msa.getMessage("app.resource.not.available"));
        }
        model.addAttribute("news", news);
        model.addAttribute("comment", new Comment());
        return NEWS_DETAIL_VIEW;
    }

    @RequestMapping(COMMON_UNIVERSITY_URL)
    public String showUniversity(@PathVariable long id, Model model) {
        University university = universityService.findById(id);
        model.addAttribute("university", university);
        model.addAttribute("newsList", newsService.findPublishedNewsByUniversity(university));
        return UNIVERSITY_DETAIL_WITH_NEWS_VIEW;
    }

    @RequestMapping("/p/images/{id}")
    public void getImageAsByteArray(@PathVariable long id, HttpServletResponse response) throws IOException {
        News news = newsService.findBy(id);
        InputStream inputStream = new ByteArrayInputStream(news.getImage());
        response.setContentType(MediaType.IMAGE_JPEG_VALUE);
        IOUtils.copy(inputStream, response.getOutputStream());
    }

    private void setReferenceData(Model model) {
        List<News> topThreeNews = newsService.findTopThreeNews();
        model.addAttribute("newsList", topThreeNews);
        model.addAttribute("news", new News());
    }
}
