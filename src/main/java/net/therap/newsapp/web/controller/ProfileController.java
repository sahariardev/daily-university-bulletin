package net.therap.newsapp.web.controller;

import net.therap.newsapp.cmd.PasswordCmd;
import net.therap.newsapp.cmd.ProfileCmd;
import net.therap.newsapp.constant.ApplicationConstant;
import net.therap.newsapp.domain.User;
import net.therap.newsapp.service.UserService;
import net.therap.newsapp.web.validator.PasswordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import static net.therap.newsapp.constant.URL.*;

/**
 * @author sahariar.alam
 * @since 10/14/19
 */
@Controller
@RequestMapping(PROFILE)
public class ProfileController {

    private static final String USER_DETAIL_VIEW = "userDetail";

    private static final String CHANGE_USER_INFO_FORM = "changeUserInfoForm";

    private static final String PASSWORD_CHANGE_FORM = "passwordChangeForm";

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordValidator passwordValidator;

    @Autowired
    private MessageSourceAccessor msa;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @RequestMapping(method = RequestMethod.GET)
    public String userProfile(HttpSession session, Model model) {
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        user = userService.findByEmail(user.getEmail());
        model.addAttribute("user", user);
        return USER_DETAIL_VIEW;
    }

    @RequestMapping(value = USER_CHANGE_INFO, method = RequestMethod.GET)
    public String changeInfo(Model model, HttpSession session) {
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        user = userService.findByEmail(user.getEmail());
        ProfileCmd profile = new ProfileCmd();
        profile.setAddress(user.getAddress());
        profile.setFirstName(user.getFirstName());
        profile.setLastName(user.getLastName());
        model.addAttribute("profile", profile);
        return CHANGE_USER_INFO_FORM;
    }

    @RequestMapping(value = USER_CHANGE_INFO, method = RequestMethod.POST)
    public String changeInfo(@Valid @ModelAttribute("profile") ProfileCmd profile,
                             Errors errors,
                             HttpSession session,
                             RedirectAttributes redirectAttributes) {
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        if (errors.hasErrors()) {
            return CHANGE_USER_INFO_FORM;
        }
        userService.updateUser(profile, user.getEmail());
        user = userService.findByEmail(user.getEmail());
        session.setAttribute("user", user);
        redirectAttributes.addFlashAttribute("message", msa.getMessage("app.successfully.updated"));
        return REDIRECT + PROFILE;
    }

    @RequestMapping(value = USER_CHANGE_PASSWORD, method = RequestMethod.GET)
    public String changePassword(HttpSession session, Model model) {
        User user = (User) session.getAttribute(ApplicationConstant.USER);
        PasswordCmd passwordCmd = new PasswordCmd();
        passwordCmd.setEmail(user.getEmail());
        model.addAttribute("password", passwordCmd);
        return PASSWORD_CHANGE_FORM;
    }

    @RequestMapping(value = USER_CHANGE_PASSWORD, method = RequestMethod.POST)
    public String changePassword(@Valid @ModelAttribute("password") PasswordCmd password,
                                 Errors errors,
                                 RedirectAttributes redirectAttributes,
                                 HttpSession session) {
        passwordValidator.validate(password, errors);
        if (errors.hasErrors()) {
            return PASSWORD_CHANGE_FORM;
        }
        User user = (User) session.getAttribute("user");
        userService.changePassword(user.getEmail(), password.getCurrentPassword(), password.getNewPassword());
        redirectAttributes.addFlashAttribute("message", msa.getMessage("app.successfully.updated"));
        return REDIRECT + PROFILE;
    }
}
