package net.therap.newsapp.web.editor;

import net.therap.newsapp.domain.Tag;
import net.therap.newsapp.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author sahariar.alam
 * @since 10/2/19
 */
@Component
public class TagEditor extends PropertyEditorSupport {

    @Autowired
    private TagService tagService;

    @Override
    public String getAsText() {
        if (Objects.isNull(getValue())) {
            return super.getAsText();
        }
        List<Tag> tags = (List<Tag>) getValue();
        if (tags.size() == 0) {
            return "";
        }
        String out = "";
        for (Tag tag : tags) {
            out = out + tag.getId() + ",";
        }
        return out.substring(0, out.length() - 1);
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (text.isEmpty()) {
            setValue(null);
        } else {
            List<Long> tagIds = Arrays.stream(text.split(","))
                    .map(tagId -> Long.parseLong(tagId.trim())).collect(Collectors.toList());
            if (tagIds.size() != 0) {
                List<Tag> tags = tagService.findAllFromGivenIds(tagIds);
                setValue(tags);
            }
        }
    }
}
