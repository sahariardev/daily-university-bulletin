package net.therap.newsapp.web.editor;

import org.springframework.stereotype.Component;

import java.beans.PropertyEditorSupport;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

/**
 * @author sahariar.alam
 * @since 10/14/19
 */
@Component
public class DateEditor extends PropertyEditorSupport {

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (!text.isEmpty()) {
            setValue(Date.valueOf(text));
        }
    }

    @Override
    public String getAsText() {
        if (Objects.nonNull(getValue()) && getValue().getClass() == Timestamp.class) {
            return (new Date(((Timestamp) getValue()).getTime())).toString();
        }
        return super.getAsText();
    }
}
