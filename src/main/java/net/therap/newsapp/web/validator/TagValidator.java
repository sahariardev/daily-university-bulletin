package net.therap.newsapp.web.validator;

import net.therap.newsapp.domain.Tag;
import net.therap.newsapp.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author sahariar.alam
 * @since 10/1/19
 */
@Component
public class TagValidator implements Validator {

    @Autowired
    private TagService tagService;

    @Autowired
    private MessageSourceAccessor msa;

    @Override
    public boolean supports(Class<?> clazz) {
        return Tag.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Tag tag = (Tag) target;
        boolean tagAlreadyExist = tagService.tagTitleExist(tag.getTitle());
        if (tagAlreadyExist) {
            errors.rejectValue("title", null, msa.getMessage("app.already.exist"));
        }
    }
}
