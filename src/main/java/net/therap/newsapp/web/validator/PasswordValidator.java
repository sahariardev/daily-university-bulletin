package net.therap.newsapp.web.validator;

import net.therap.newsapp.cmd.PasswordCmd;
import net.therap.newsapp.domain.User;
import net.therap.newsapp.helper.PasswordEncoderHelper;
import net.therap.newsapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * @author sahariar.alam
 * @since 10/2/19
 */
@Component
public class PasswordValidator implements Validator {

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSourceAccessor msa;

    @Autowired
    private PasswordEncoderHelper passwordEncoderHelper;

    @Override
    public boolean supports(Class<?> clazz) {
        return PasswordCmd.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        PasswordCmd password = (PasswordCmd) target;
        User user = userService.findByEmail(password.getEmail());
        if (!passwordEncoderHelper.hash(password.getCurrentPassword()).equals(user.getPassword())) {
            errors.rejectValue("oldPassword", null, msa.getMessage("app.wrong.password"));
        }
    }
}
