package net.therap.newsapp.web.validator;

import net.therap.newsapp.domain.University;
import net.therap.newsapp.service.UniversityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Date;

/**
 * @author sahariar.alam
 * @since 10/1/19
 */
@Component
public class UniversityValidator implements Validator {

    @Autowired
    private UniversityService universityService;

    @Autowired
    private MessageSourceAccessor msa;

    @Override
    public boolean supports(Class<?> clazz) {
        return University.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        University university = (University) target;
        Date date = university.getFoundation();
        Date currentDate = new Date();

        if (universityService.universityTitleExist(university.getTitle())) {
            errors.rejectValue("title", null, msa.getMessage("app.already.exist"));
        }

        if (date != null && !date.before(currentDate)) {
            errors.rejectValue("foundation", null, msa.getMessage("app.foundation.date"));
        }
    }
}
