package net.therap.newsapp.web.validator;

import net.therap.newsapp.cmd.LoginCmd;
import net.therap.newsapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;

/**
 * @author sahariar.alam
 * @since 9/30/19
 */
@Component
public class LoginCmdValidator implements Validator {

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSourceAccessor msa;

    @Override
    public boolean supports(Class<?> clazz) {
        return LoginCmd.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        LoginCmd loginCmd = (LoginCmd) target;
        boolean isValidUser = userService.isValidUser(loginCmd.getEmail(), loginCmd.getPassword());
        if (!isValidUser && Objects.nonNull(loginCmd.getEmail())) {
            errors.rejectValue("email", null, msa.getMessage("invalid.email.password"));
        }
    }
}
