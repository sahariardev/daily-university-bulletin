package net.therap.newsapp.web.validator;

import net.therap.newsapp.domain.User;
import net.therap.newsapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author sahariar.alam
 * @since 10/9/19
 */
@Component
public class UserValidator implements Validator {

    private static final String EMAIL_REGEX = "^(.+)@(.+)$";

    private static final Pattern EMAIL_PATTERN = Pattern.compile(EMAIL_REGEX);

    @Autowired
    private UserService userService;

    @Autowired
    private MessageSourceAccessor msa;

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        User user = (User) target;
        if (Objects.nonNull(user) && Objects.nonNull(user.getEmail())) {
            Matcher matcher = EMAIL_PATTERN.matcher(user.getEmail());
            if (!matcher.matches()) {
                errors.rejectValue("email", null, msa.getMessage("app.invalid.email"));
            }
        }
        if (user.getUniversity().getId() == 0) {
            errors.rejectValue("university.id", null, msa.getMessage("app.select.university"));
        }
        if (userService.checkEmailExist(user.getEmail())) {
            errors.rejectValue("email", null, msa.getMessage("app.already.exist"));
        }
    }
}
