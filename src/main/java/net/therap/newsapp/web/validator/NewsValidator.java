package net.therap.newsapp.web.validator;

import net.therap.newsapp.domain.News;
import net.therap.newsapp.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.sql.Date;
import java.util.Calendar;
import java.util.Objects;

/**
 * @author sahariar.alam
 * @since 10/2/19
 */
@Component
public class NewsValidator implements Validator {

    @Autowired
    private NewsService newsService;

    @Autowired
    private MessageSourceAccessor msa;

    @Override
    public boolean supports(Class<?> clazz) {
        return News.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        News news = (News) target;
        Calendar calender = Calendar.getInstance();
        calender.add(Calendar.DATE, -1);
        Date previousDate = new Date(calender.getTimeInMillis());
        checkImage(news, errors);
        checkPublishDate(news, previousDate, errors);
        checkEndDate(news, previousDate, errors);
        checkPublishAndEndDate(news, errors);
        checkNewsTitleAlreadyExist(news, errors);
    }

    private void checkNewsTitleAlreadyExist(News news, Errors errors) {
        if (newsService.checkNewsTitleAlreadyExist(news)) {
            errors.rejectValue("title", null, msa.getMessage("app.title.exist"));
        }
    }

    private void checkImage(News news, Errors errors) {
        if (!news.getImageFile().isEmpty() && !news.getImageFile().getContentType().equals("image/jpeg")) {
            errors.rejectValue("imageFile", null, msa.getMessage("app.file.jpg.allowed"));
        }
    }

    private void checkPublishDate(News news, Date currentDate, Errors errors) {
        if (Objects.isNull(news.getPublishDate())) {
            errors.rejectValue("publishDate", null, msa.getMessage("app.not.empty"));
        }
        if (Objects.nonNull(news.getPublishDate()) && news.getPublishDate().before(currentDate)) {
            errors.rejectValue("publishDate", null, msa.getMessage("app.past.date"));
        }
    }

    private void checkEndDate(News news, Date currentDate, Errors errors) {
        if (Objects.isNull(news.getEndDate())) {
            errors.rejectValue("endDate", null, msa.getMessage("app.not.empty"));
        }

        if (Objects.nonNull(news.getEndDate()) && news.getEndDate().before(currentDate)) {
            errors.rejectValue("endDate", null, msa.getMessage("app.past.date"));
        }
    }

    private void checkPublishAndEndDate(News news, Errors errors) {
        if (Objects.nonNull(news.getPublishDate()) && Objects.nonNull(news.getEndDate())
                && news.getEndDate().before(news.getPublishDate())) {
            errors.rejectValue("endDate", null, msa.getMessage("app.end.date.before.publish.date"));
        }
    }
}
