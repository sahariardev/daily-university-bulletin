<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <decorator:title/>
    </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<c:url value="/css/bootstrap.min.css"/>">
    <link rel="stylesheet" href="<c:url value="/css/style.css" />">
    <script src="<c:url value=" /js/jquery-3.4.1.min.js " />"></script>
    <script src="<c:url value=" /js/bootstrap.min.js " />"></script>
</head>
<body>
<div id="header">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item active">
                    <a class="nav-link" href="/p/home/">
                        <spring:message code="app.site.title"/>
                        <span class="sr-only">(current)</span></a>
                </li>
            </ul>
        </div>
        <div class="my-2 my-lg-0 input-group-lg">
            <c:if test="${sessionScope.user == null}">
                <a class="btn btn-info" href="/p/auth/login">
                    <spring:message code="app.site.login"/>
                </a>
            </c:if>
            <c:if test="${sessionScope.user != null}">
                <a class="welcome">
                    <spring:message code="app.welcome"/>
                    <c:out value="${sessionScope.user.lastName}"></c:out>
                </a>

                <div class="display-block">
                    <a class="btn btn-info" href="/a/profile">
                        <spring:message code="app.site.dashboard"/>
                    </a>
                </div>
                <div class="display-block">
                    <form:form method="post" action="/p/auth/logout">
                        <button class="btn btn-info" type="submit">
                            <spring:message code="app.site.logout"/>
                        </button>
                    </form:form>
                </div>
            </c:if>
        </div>
    </nav>
</div>
<div id="main" class="container main-container">
    <div class="row">
        <div class="col-md-2">
            <div class="menu_container">
                <c:forEach var="item" items="${menuItems}">
                    <div class="item">
                        <a href="<c:out value=" ${item.url} "></c:out>">
                            <c:out value="${item.displayName}"></c:out>
                        </a>
                    </div>
                </c:forEach>
            </div>
        </div>
        <div class="col-md-8 admin-body">
            <decorator:body/>
        </div>
    </div>
</div>
</body>
</html>