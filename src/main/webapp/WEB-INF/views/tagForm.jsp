<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <spring:message code="app.tag"/>
    </title>
</head>
<body>
<div class="">
    <div class="page-title">
        <spring:message code="app.create.tag"/>
    </div>
    <div class="mar-bottom-30">
        <div>
            <a href="/a/tags" class="btn black-bg-btn">
                <spring:message code="app.back"/>
            </a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <c:if test="${message != null}">
                <div class="alert alert-info max-w-400">
                    <c:out value="${message}"></c:out>
                </div>
            </c:if>
            <form:form method="post" modelAttribute="tag">
                <div class="form-group">
                    <div class="form-group">
                        <label>
                            <spring:message code="app.title"/>
                        </label>
                        <form:input type="text" class="form-control" path="title"/>
                        <div>
                            <form:errors path="title" class="color-red"/>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info"><spring:message code="app.submit"/></button>
                </div>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
