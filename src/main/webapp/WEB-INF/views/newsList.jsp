<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <spring:message code="app.news"/>
    </title>
</head>
<body>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <spring:message code="app.news"/>
            </div>
            <c:if test="${message != null}">
                <div class="alert alert-info max-w-400">
                    <c:out value="${message}"></c:out>
                </div>
            </c:if>
            <div>
                <form:form action="" method="post" modelAttribute="newsFilter">
                    <div>
                        <form:errors path="page" class="color-red"/>
                    </div>
                    <div>
                        <form:errors path="limit" class="color-red"/>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <label>
                                <spring:message code="app.select.status"/> </label>
                            <form:select class="form-control" path="postStatus">
                                <option value="">
                                    <spring:message code="app.all"/>
                                </option>
                                <c:forEach var="stat" items="${status}">
                                    <option value="${stat.name()}">
                                        <c:out value="${stat.displayName}"></c:out>
                                    </option>
                                </c:forEach>
                            </form:select>
                        </div>
                        <div class="col-md-2">
                            <button type="submit" class="btn btn-info mar-top-32">
                                <spring:message code="app.apply"/>
                            </button>
                        </div>
                        <div class="col-md-7">
                            <div class="float-right mar-top-32">
                                <div>
                                    <a href="/a/news/create" class="btn black-bg-btn">
                                        <spring:message code="app.create.new"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
