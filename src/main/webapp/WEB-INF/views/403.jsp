<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>

<head>
    <title>
        <spring:message code="app.unauthorized.action"/>
    </title>
</head>

<body>
<div class="text-align-center">
    <h1><spring:message code="app.unauthorized.action"/></h1>
</div>
</body>

</html>
