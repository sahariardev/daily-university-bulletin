<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title><spring:message code="app.login"/></title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-4 box-shadow card-back">
            <div class="page-title">
                <spring:message code="app.login"/>
            </div>
            <c:if test="${message != null}">
                <div class="alert alert-info">
                    <c:out value="${message}"></c:out>
                </div>
            </c:if>
            <form:form method="post" modelAttribute="login">
                <div class="form-group">
                    <div class="card-width">
                        <form:errors cssClass="alert alert-danger" path="email"/>
                    </div>
                    <div class="form-group">
                        <label><spring:message
                                code="app.email"/></label>
                        <form:input type="email" class="form-control" path="email"/>
                    </div>
                    <div class="form-group">
                        <label><spring:message
                                code="app.password"/></label>
                        <form:input type="password" class="form-control" path="password"/>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary"><spring:message code="app.submit"/></button>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
