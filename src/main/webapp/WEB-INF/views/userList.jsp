<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <spring:message code="app.user"/>
    </title>
</head>
<body>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <spring:message code="app.user.list"/>
            </div>
            <div>
                <form:form action="" method="post" modelAttribute="userCmd">

                    <div class="row">
                        <div class="col-md-3">
                            <label>
                                <spring:message code="app.select.status"/> </label>
                            <form:select class="form-control" path="accountStatus">
                                <option value="">
                                    <spring:message code="app.all"/>
                                </option>
                                <c:forEach var="stat" items="${status}">
                                    <option value="${stat.name()}">
                                        <c:out value="${stat.displayName}"></c:out>
                                    </option>
                                </c:forEach>
                            </form:select>
                        </div>
                        <div class="col-md-3">
                            <label>
                                <spring:message code="app.select.role"/> </label>
                            <form:select class="form-control" path="role.title">
                                <option value="">
                                    <spring:message code="app.all"/>
                                </option>
                                <c:forEach var="role" items="${roles}">
                                    <option value="${role.name()}">
                                        <c:out value="${role.displayName}"></c:out>
                                    </option>
                                </c:forEach>
                            </form:select>
                        </div>
                        <div class="col-md-3">
                            <button type="submit" class="btn btn-info mar-top-32">
                                <spring:message code="app.apply"/>
                            </button>
                        </div>
                    </div>
                </form:form>
            </div>
            <c:if test="${users != null && users.size() != 0}">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">
                            <spring:message code="app.name"/>
                        </th>
                        <th scope="col">
                            <spring:message code="app.email"/>
                        </th>
                        <th scope="col">
                            <spring:message code="app.address"/>
                        </th>
                        <th scope="col">
                            <spring:message code="app.account.status"/>
                        </th>
                        <th scope="col">
                            <spring:message code="app.role"/>
                        </th>
                        <th scope="col">
                            <spring:message code="app.apply"/>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <c:forEach var="user" items="${users}">
                        <c:if test="${user.email.equals(sessionScope.user.email)}">
                    <tr>
                        <form:form action="/a/users/all" method="post" modelAttribute="userModel">
                            <td>
                                <div>
                                    <c:out value="${user.firstName}"></c:out>
                                    <c:out value="${user.lastName}"></c:out>
                                </div>
                            </td>
                            <td>
                                <form:input type="hidden" path="email" value="${user.email}"/>
                                <div>
                                    <c:out value="${user.email}"></c:out>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <c:out value="${user.address}"></c:out>
                                </div>
                            </td>
                            <td>
                                <form:select class="form-control" path="accountStatus">
                                    <c:forEach var="stat" items="${status}">
                                        <option value="${stat.name()}"
                                                <c:if test="${user.accountStatus.name().equals(stat.name())}"> selected
                                                </c:if>>
                                            <c:out value="${stat.name()}"></c:out>
                                        </option>
                                    </c:forEach>
                                </form:select>

                            </td>
                            <td>
                                <form:select class="form-control" path="role.title">
                                    <c:forEach var="role" items="${roles}">
                                        <option value="${role.name()}"
                                                <c:if test="${user.role.title.equals(role.name())}"> selected </c:if> >
                                            <c:out value="${role.name()}"></c:out>
                                        </option>
                                    </c:forEach>
                                </form:select>

                            </td>
                            <td>
                                <div>
                                    <button type="submit" class="btn btn-info">
                                        <spring:message code="app.apply"/>
                                    </button>
                                </div>
                            </td>
                        </form:form>
                    </tr>
                    </c:if>
                    </c:forEach>
                    </tr>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</div>
</body>
</html>
