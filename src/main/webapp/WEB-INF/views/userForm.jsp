<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <spring:message code="app.user"/>
    </title>
</head>
<body>
<div class="">
    <div class="row">
        <div class="col-md-4 box-shadow card-back">
            <div class="page-title">
                <spring:message code="app.sign.up"/>
            </div>
            <form:form method="post" modelAttribute="user">
                <div class="form-group">
                    <div class="form-group">
                        <label>
                            <spring:message code="app.email"/>
                        </label>
                        <form:input type="text" class="form-control" path="email"/>
                        <div>
                            <form:errors path="email" class="color-red"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.first.name"/>
                        </label>
                        <form:input type="text" class="form-control" path="firstName"/>
                        <div>
                            <form:errors path="firstName" class="color-red"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.last.name"/>
                        </label>
                        <form:input type="text" class="form-control" path="lastName"/>
                        <div>
                            <form:errors path="lastName" class="color-red"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.university"/>
                        </label>
                        <form:select class="form-control" path="university.id">
                            <option selected value="0"> -- select an option --</option>
                            <c:forEach var="uni" items="${universities}">
                                <option value="${uni.id}"
                                        <c:if test="${uni.id == user.university.id}">selected </c:if> >
                                    <c:out value="${uni.title}"></c:out>
                                </option>
                            </c:forEach>
                        </form:select>
                        <div>
                            <form:errors path="university.id" class="color-red"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.password"/>
                        </label>
                        <form:input type="password" class="form-control" path="password"/>
                        <div>
                            <form:errors path="password" class="color-red"/>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">
                    <spring:message code="app.submit"/>
                </button>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
