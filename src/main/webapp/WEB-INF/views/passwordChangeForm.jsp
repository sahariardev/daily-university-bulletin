<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <spring:message code="app.user"/>
    </title>
</head>
<body>
<div class="">
    <div class="row">
        <div class="col-md-6">
            <form:form method="post" modelAttribute="password">
                <div class="form-group">
                    <form:input type="hidden" class="form-control" path="email"/>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.current.password"/>
                        </label>
                        <form:input type="password" class="form-control" path="currentPassword"/>
                        <div>
                            <form:errors path="currentPassword" class="color-red"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.new.password"/>
                        </label>
                        <form:input type="password" class="form-control" path="newPassword"/>
                        <div>
                            <form:errors path="newPassword" class="color-red"/>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">
                    <spring:message code="app.submit"/>
                </button>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
