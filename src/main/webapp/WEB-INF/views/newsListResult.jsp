<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <spring:message code="app.news"/>
    </title>
</head>
<body>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <spring:message code="app.news.list"/>
            </div>
            <div>
                <div class="float-left mar-bottom-30">
                    <div>
                        <a href="/a/news" class="btn black-bg-btn">
                            <spring:message code="app.back"/>
                        </a>
                    </div>
                </div>
                <div class="float-right mar-bottom-30">
                    <div>
                        <a href="/a/news/create" class="btn black-bg-btn">
                            <spring:message code="app.create.new"/>
                        </a>
                    </div>
                </div>
            </div>
            <c:if test="${news == null || news.size() == 0}">
                <div class="message-container">
                    <spring:message code="app.no.result.found"/>
                </div>
            </c:if>
            <c:if test="${news != null && news.size() != 0}">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">
                            <spring:message code="app.id"/>
                        </th>
                        <th scope="col">
                            <spring:message code="app.title"/>
                        </th>
                        <th scope="col">
                            <spring:message code="app.author"/>
                        </th>
                        <th scope="col">
                            <spring:message code="app.university"/>
                        </th>
                        <th scope="col">
                            <spring:message code="app.status"/>
                        </th>
                        <th scope="col">
                            <spring:message code="app.action"/>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <c:forEach var="n" items="${news}">
                    <tr>
                        <td>
                            <div>
                                <c:out value="${n.id}"></c:out>
                            </div>
                        </td>
                        <td>
                            <div>
                                <c:out value="${n.title}"></c:out>
                            </div>
                        </td>
                        <td>
                            <div>
                                <c:out value="${n.createdBy.email}"></c:out>
                            </div>
                        </td>
                        <td>
                            <div>
                                <c:out value="${n.university.title}"></c:out>
                            </div>
                        </td>
                        <td>
                            <div>
                                <c:out value="${n.status}"></c:out>
                            </div>
                        </td>
                        <td>
                            <div>
                                <a class="btn btn-info" href="/a/news/edit/${n.id}">
                                    <spring:message code="app.view"/>
                                </a>
                            </div>
                        </td>
                    </tr>
                    </c:forEach>
                    </tr>
                    </tbody>
                </table>

                <c:if test="${totalNews>10}">
                    <div class="pagination-container">
                        <c:forEach var="i" begin="0" end="${totalNews/10}" step="1" varStatus="loop">
                            <form:form class="pagination" action="" method="post" modelAttribute="newsFilter">
                                <form:input path="postStatus" type="hidden"></form:input>
                                <form:input path="page" type="hidden" value="${i+1}"></form:input>
                                <input type="submit"
                                       <c:if test="${newsFilter.page == i+1}">class="btn btn-dark"</c:if>
                                       <c:if test="${newsFilter.page != i+1}">class="btn btn-light"</c:if>
                                       value="${i+1}"/>
                            </form:form>
                        </c:forEach>
                    </div>
                </c:if>
            </c:if>
        </div>
    </div>
</div>
</body>
</html>
