<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>
        <spring:message code="app.news"/>
    </title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-10">
            <div class="news-title">
                <c:out value="${news.title}"></c:out>
            </div>
            <div class="auth-name">
                <spring:message code="app.written"/>
                <c:out value="${news.createdBy.lastName} "></c:out> on
                <fmt:formatDate type="date" value="${news.publishDate}"/>
            </div>
            <c:if test="${news.image !=null}">
                <div class="news-image">
                    <img class="img-responsive" src="<c:url value=" /p/images/${news.id} " />">
                </div>
            </c:if>

            <div class="news-body">
                <c:out value="${news.body}" escapeXml="false"></c:out>
            </div>
        </div>
        <div class="col-md-2">
            <div class="tag-container">
                <c:forEach var="tag" items="${news.tags}">
                    <div class="tag-item">
                        <spring:message code="app.hash"/>
                        <c:out value="${tag.title}"></c:out>
                    </div>
                </c:forEach>
            </div>
            <div class="news-university">
                <a class="white" href="/p/university/<c:out value=" ${news.university.id} "></c:out>">
                    <c:out value="${news.university.title}"></c:out>
                </a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <c:if test="${sessionScope.user != null && !user.banned}">
                <div class="comment-form">
                    <div>
                        <spring:message code="app.write.comment"/>
                    </div>
                    <form:form action="/a/news/${news.id}/comment" method="post" modelAttribute="comment">
                        <div class="form-group">
                            <form:textarea path="body" class="form-control" rows="5"></form:textarea>
                            <form:input type="hidden" path="id" class="form-control" value="0"/>
                            <form:errors path="body" class="color-red"/>
                            <c:if test="${error != null}">
                                <div class="color-red">
                                    <c:out value="${error}"></c:out>
                                </div>
                            </c:if>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-info">
                                <spring:message code="app.submit"/>
                            </button>
                        </div>
                    </form:form>
                </div>
            </c:if>
            <c:if test="${news.comments.size()!=0}">
                <div class="comment-container">
                    <div>
                        <h3>
                            <spring:message code="app.comments"/>
                        </h3>
                    </div>
                    <c:forEach var="comment" items="${news.comments}">
                        <c:if test="${!comment.createdBy.banned}">
                            <div class="comment">
                                <div class="comment-body">
                                    <c:out value="${comment.body}"></c:out>
                                </div>
                                <div class="comment-author">
                                    <spring:message code="app.written"/>
                                    <c:out value="${comment.createdBy.lastName}"></c:out>
                                </div>
                                <div class="comment-date">
                                    <fmt:formatDate type="date" value="${comment.created}"/>
                                </div>
                            </div>
                        </c:if>
                    </c:forEach>
                </div>
            </c:if>
        </div>
    </div>
</div>
</body>
</html>
