<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <spring:message code="app.university"/>
    </title>
</head>
<body>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <spring:message code="app.university.list"/>
            </div>
            <c:if test="${message != null}">
                <div class="alert alert-info max-w-400">
                    <c:out value="${message}"></c:out>
                </div>
            </c:if>
            <div class="float-right mar-bottom-30">
                <div>
                    <a href="/a/universities/create" class="btn black-bg-btn">
                        <spring:message code="app.create.new"/> </a>
                </div>
            </div>
            <c:if test="${universities != null && universities.size() != 0}">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">
                            <spring:message code="app.id"/>
                        </th>
                        <th scope="col">
                            <spring:message code="app.title"/>
                        </th>
                        <th scope="col">
                            <spring:message code="app.action"/>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <c:forEach var="university" items="${universities}">
                    <tr>
                        <td>
                            <div>
                                <c:out value="${university.id}"></c:out>
                            </div>
                        </td>
                        <td>
                            <div>
                                <c:out value="${university.title}"></c:out>
                            </div>
                        </td>
                        <td>
                            <div>
                                <a class="btn btn-info"
                                   href="/a/universities/<c:out value=" ${university.id} "></c:out>">
                                    <spring:message code="app.view"/> </a>
                            </div>
                        </td>
                    </tr>
                    </c:forEach>
                    </tr>
                    </tbody>
                </table>
            </c:if>
        </div>
    </div>
</div>
</body>
</html>
