<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <spring:message code="app.home"/>
    </title>
</head>
<body>
<div class="container">
    <div class="top-news-section">
        <h2 class="title text-align-center mar-bottom-30">
            <spring:message code="app.featured.news"/>
        </h2>

        <div class="row">
            <c:forEach var="news" items="${newsList}">
                <div class="col-md-4">
                    <div class="card card-width">
                        <c:if test="${news.image==null}">
                            <img src="/images/no_image.jpg" class="card-img-top">
                        </c:if>
                        <c:if test="${news.image!=null}">
                            <img src="/p/images/<c:out value=" ${news.id}"/>" class="card-img-top">
                        </c:if>
                        <div class="card-body">
                            <h5 class="card-title"><c:out value="${news.title}"/></h5>
                            <a href="/p/news/<c:out value=" ${news.id} "/>" class="btn  black-bg-btn"><spring:message
                                    code="app.view"/></a>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>
    <div class="search-form">
        <form:form action="" method="post" modelAttribute="news">
            <div class="form-group">
                <form:input type="text" class="form-control search-form-input" path="title" placeholder="Search news"/>
            </div>
            <button type="submit" id="search-btn" class="btn black-bg-btn">
                <spring:message code="app.search"/>
            </button>
        </form:form>
    </div>
    <div class="search-result" id="search-result">
        <c:if test="${result!=null && result.size()==0}">
            <div class="message-container">
                <spring:message code="app.no.result.found"/>
            </div>
        </c:if>
        <c:forEach var="news" items="${result}">
            <div class="news-container">
                <div class="news-title-search">
                    <c:out value="${news.title}"/>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="news-author-search left-pad">
                            <spring:message code="app.author"/> :
                            <c:out value="${news.createdBy.lastName}"/>
                        </div>
                        <div class="news-university-search left-pad">
                            <spring:message code="app.university"/> :
                            <c:out value="${news.university.title}"/>
                        </div>
                        <div class="news-university-search left-pad">
                            <spring:message code="app.published.on"/> :
                            <c:out value="${news.publishDate}"/>
                        </div>
                    </div>
                    <div class="col-md-6 button-pad">
                        <a href="/p/news/<c:out value=" ${news.id}"/>" class="btn black-bg-btn">
                            <spring:message code="app.view"/>
                        </a>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
</body>
</html>
