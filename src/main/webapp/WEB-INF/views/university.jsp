<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>
        <c:out value="${university.title}"></c:out>
    </title>
</head>
<body>
<div class="">
    <c:if test="${message != null}">
        <div class="alert alert-info max-w-400">
            <c:out value="${message}"></c:out>
        </div>
    </c:if>
    <h1>
        <c:out value="${university.title}"></c:out>
    </h1>

    <div class="row">
        <div class="col-md-6">
            <div class="address-container mar-bottom-30">
                <h6>
                    <spring:message code="app.address"/>
                </h6>

                <div>
                    <c:out value="${university.address}"></c:out>
                </div>
            </div>
            <div class="number-container">
                <h6>
                    <spring:message code="app.contact.number"/>
                </h6>

                <div>
                    <c:out value="${university.number}"></c:out>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div>
                <h6><spring:message code="app.foundation"/></h6>

                <div>
                    <fmt:formatDate type="date" value="${university.foundation}"/>
                </div>

                <a class="btn btn-info" target="_blank" href="<c:out value=" ${university.url} "></c:out>">
                    <spring:message code="app.official.website"/>
                </a>
                <a class="btn btn-info" href="edit/<c:out value=" ${university.id} "></c:out>">
                    <spring:message code="app.update.info"/>
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
