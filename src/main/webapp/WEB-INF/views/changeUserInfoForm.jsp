<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <spring:message code="app.user"/>
    </title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <form:form method="post" modelAttribute="profile">
                <div class="form-group">
                    <div class="form-group">
                        <label>
                            <spring:message code="app.first.name"/>
                        </label>
                        <form:input type="text" class="form-control" path="firstName"/>
                        <div>
                            <form:errors path="firstName" class="color-red"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.last.name"/>
                        </label>
                        <form:input type="text" class="form-control" path="lastName"/>
                        <div>
                            <form:errors path="lastName" class="color-red"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.address"/>
                        </label>
                        <form:input type="text" class="form-control" path="address"/>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">
                    <spring:message code="app.submit"/>
                </button>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
