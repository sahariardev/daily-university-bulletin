<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <spring:message code="app.university"/>
    </title>
</head>
<body>
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="page-title">
                <spring:message code="app.create.new.university"/>
            </div>
            <div class="mar-bottom-30">
                <div>
                    <a href="/a/universities" class="btn black-bg-btn">
                        <spring:message code="app.back"/>
                    </a>
                </div>
            </div>
            <c:if test="${message != null}">
                <div class="alert alert-info max-w-400">
                    <c:out value="${message}"></c:out>
                </div>
            </c:if>
            <form:form method="post" modelAttribute="university">
                <div class="form-group">

                    <div class="form-group">
                        <label>
                            <spring:message code="app.title"/>
                        </label>
                        <form:input type="text" class="form-control" path="title"/>
                        <div>
                            <form:errors path="title" class="color-red"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.address"/>
                        </label>
                        <form:input type="text" class="form-control" path="address"/>
                        <div>
                            <form:errors path="address" class="color-red"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.url"/>
                        </label>
                        <form:input type="text" class="form-control" path="url"/>
                        <div>
                            <form:errors path="url" class="color-red"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.foundation"/>
                        </label>
                        <form:input type="date" class="form-control" path="foundation"/>
                        <div>
                            <form:errors path="foundation" class="color-red"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.number"/>
                        </label>
                        <form:input type="tel" class="form-control" path="number"/>
                        <div>
                            <form:errors path="number" class="color-red"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.latitude"/>
                        </label>
                        <form:input type="text" class="form-control" path="lat"/>
                        <div>
                            <form:errors path="lat" class="color-red"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>
                            <spring:message code="app.longitude"/>
                        </label>
                        <form:input type="text" class="form-control" path="lon"/>
                        <div>
                            <form:errors path="lon" class="color-red"/>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-info"><spring:message code="app.submit"/></button>
                </div>
            </form:form>
        </div>
    </div>
</div>
</body>
</html>
