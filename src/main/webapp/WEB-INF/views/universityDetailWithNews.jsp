<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>
        <c:out value="${university.title}"></c:out>
    </title>
</head>
<body>
<div>
    <c:if test="${message != null}">
        <div class="alert alert-info max-w-400">
            <c:out value="${message}"></c:out>
        </div>
    </c:if>
    <div class="university-container">
        <h1 class="title">
            <c:out value="${university.title}"></c:out>
        </h1>

        <div class="row">
            <div class="col-md-6">
                <div class="address-container mar-bottom-30">
                    <h6>
                        <spring:message code="app.address"/>
                    </h6>

                    <div>
                        <c:out value="${university.address}"></c:out>
                    </div>
                </div>
                <div class="number-container">
                    <h6>
                        <spring:message code="app.contact.number"/>
                    </h6>

                    <div>
                        <c:out value="${university.number}"></c:out>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div>
                    <h6><spring:message code="app.foundation"/></h6>

                    <div>
                        <fmt:formatDate type="date" value="${university.foundation}"/>
                    </div>
                    <a target="_blank" class="btn btn-info" href="<c:out value=" ${university.url} "></c:out>">
                        <spring:message code="app.official.website"/>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="search-result">
        <h3 class="text-align-center">
            <spring:message code="app.news"/>
        </h3>
        <c:forEach var="news" items="${newsList}">
            <div class="news-container">
                <div class="news-title-search">
                    <c:out value="${news.title}"></c:out>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="news-author-search left-pad">
                            <spring:message code="app.author"/> :
                            <c:out value="${news.createdBy.lastName}"></c:out>
                        </div>
                        <div class="news-university-search left-pad">
                            <spring:message code="app.university"/> :
                            <c:out value="${news.university.title}"></c:out>
                        </div>
                        <div class="news-university-search left-pad">
                            <spring:message code="app.published.on"/> :
                            <c:out value="${news.publishDate}"></c:out>
                        </div>
                    </div>
                    <div class="col-md-6 button-pad">
                        <a href="/p/news/<c:out value=" ${news.id} "></c:out>" class="btn  black-bg-btn">
                            <spring:message code="app.view"/> </a>
                    </div>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
</body>
</html>
