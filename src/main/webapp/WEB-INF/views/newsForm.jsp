<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>
        <spring:message code="app.news"/>
    </title>
</head>
<body>
<div class="container">
    <div class="col-md-12">
        <c:if test="${news.id!=0}">
            <div class="page-title">
                <spring:message code="app.update.news"/>
            </div>
        </c:if>
        <c:if test="${news.id==0}">
            <div class="page-title">
                <spring:message code="app.create.news"/>
            </div>
        </c:if>
        <div class="row">
            <div class="col-md-6">
                <div class="mar-bottom-30">
                    <div>
                        <a href="/a/news" class="btn black-bg-btn">
                            <spring:message code="app.back"/>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <c:if test="${news.id!=0}">
                    <div class="mar-bottom-30 float-right">
                        <a href="/a/news/create" class="btn black-bg-btn">
                            <spring:message code="app.create.new"/>
                        </a>
                    </div>
                </c:if>
            </div>
        </div>
        <c:if test="${message != null}">
            <div class="alert alert-info max-w-400">
                <c:out value="${message}"></c:out>
            </div>
        </c:if>
        <form:form method="post" modelAttribute="news" enctype="multipart/form-data">
            <div class="form-group">
                <div class="form-group">
                    <label>
                        <spring:message code="app.title"/>
                    </label>
                    <form:input type="text" class="form-control" path="title"/>
                    <div>
                        <form:errors path="title" class="color-red"/>
                    </div>
                </div>
                <div class="form-group">
                    <label>
                        <spring:message code="app.body"/>
                    </label>
                    <form:textarea path="body" class="form-control" rows="5"></form:textarea>
                    <div>
                        <form:errors path="body" class="color-red"/>
                    </div>
                </div>
                <c:if test="${sessionScope.user.superAdmin}">
                    <div class="form-group">
                        <label>
                            <spring:message code="app.priority"/>
                        </label>
                        <form:input type="text" class="form-control" path="priority"/>
                        <div>
                            <form:errors path="priority" class="color-red"/>
                        </div>
                    </div>
                </c:if>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <spring:message code="app.publish.date"/>
                            </label>
                            <form:input type="date" class="form-control" path="publishDate"/>
                            <div>
                                <form:errors path="publishDate" class="color-red"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>
                                <spring:message code="app.end.date"/>
                            </label>
                            <form:input type="date" class="form-control" path="endDate"/>
                            <div>
                                <form:errors path="endDate" class="color-red"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <c:if test="${news.image!=null }">
                            <img src="<c:url value="/p/images/${news.id} "/>">
                        </c:if>
                        <div class="form-group">
                            <label>
                                <spring:message code="app.select.image"/>
                            </label>
                            <form:input type="file" path="imageFile"/>
                            <div>
                                <form:errors path="imageFile" class="color-red"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label>
                        <spring:message code="app.post.status"/>
                    </label>
                    <form:select class="form-control" path="status">
                        <option selected value="">
                            <spring:message code="app.select.an.option"/>
                        </option>
                        <c:forEach var="status" items="${availableStatus}">
                            <option value="${status.name()}" <c:if test="${news.status.equals(status)}">
                                <c:out value="selected"></c:out>
                            </c:if> >
                                <c:out value="${status.displayName}"></c:out>
                            </option>
                        </c:forEach>
                    </form:select>
                </div>
                <div class="form-group">
                    <label>
                        <spring:message code="app.tag"/>
                    </label>

                    <div class="tag-container-form">
                        <c:forEach var="tag" items="${news.tags}">
                            <div>
                                <c:out value="${tag.title}"/>
                            </div>
                        </c:forEach>
                    </div>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#tagModal">
                        <spring:message code="app.selectTags"/>
                    </button>
                    <div>
                        <form:errors path="tags" class="color-red"/>
                    </div>
                </div>
                <form:input type="hidden" class="form-control" path="tags" id="tag-input"/>

                <c:if test="${news.status == null || !news.status.name().equals('PUBLISHED') }">
                    <button type="submit" class="btn btn-info">
                        <spring:message code="app.submit"/>
                    </button>
                </c:if>
            </div>
        </form:form>
        <c:if test="${news.id!=0 && news.comments.size()!=0 }">
        <div>
            <div class="comment-container">
                <div>
                    <h3>
                        <spring:message code="app.comments"/>
                    </h3>
                </div>
                <c:forEach var="comment" items="${news.comments}">
                    <div class="comment">
                        <div class="comment-body">
                            <c:out value="${comment.body}"></c:out>
                        </div>
                        <div class="comment-author">
                            <c:out value="${comment.createdBy.lastName}"></c:out>
                        </div>
                        <div class="comment-date">
                            <fmt:formatDate type="date" value="${comment.created}"/>
                        </div>

                        <form:form action="/a/news/${news.id}/comment/delete" method="post" modelAttribute="comment">
                            <form:input type="hidden" path="id" value="${comment.id}"/>
                            <button type="button" class="btn btn-danger dlt-btn" id="s-${comment.id}">
                                <spring:message code="app.delete"/>
                            </button>
                            <button type="submit" class="btn btn-danger display-none" id="d-${comment.id}">
                            </button>
                        </form:form>
                    </div>
                </c:forEach>
            </div>
        </div>
    </div>
    </c:if>
</div>
<!-- Modal -->
<div class="modal fade" id="tagModal" tabindex="-1" role="dialog" aria-labelledby="tagModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="tagModalLabel"><spring:message code="app.available.tags"/></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <c:forEach var="tag" items="${allTags}">
                    <div class="form-check">
                        <input class="form-check-input tag-checkbox"
                               displayName="<c:out value=" ${tag.title} "></c:out>" type="checkbox"
                               value="<c:out value=" ${tag.id} "></c:out>">
                        <label class="form-check-label">
                            <c:out value="${tag.title}"></c:out>
                        </label>
                    </div>
                </c:forEach>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    <spring:message code="app.close"/>
                </button>
                <button type="button" class="btn btn-primary" id="select-tag">
                    <spring:message code="app.select"/>
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    $(function () {
        $(document).ready(function () {
            var tagsInput = $("#tag-input").val().split(",");

            for (var c = 0; c < $(".tag-checkbox").length; c++) {
                if (listContainsElem(tagsInput, $(".tag-checkbox").eq(c).val())) {
                    $(".tag-checkbox").eq(c).prop("checked", true);
                }
            }

            function listContainsElem(list, elem) {
                function checkValue(value) {
                    return value == elem;
                }

                return list.findIndex(checkValue) != -1;
            }

            $("#select-tag").click(function () {
                $('#tagModal').modal('hide');
                var tagList = [];
                var tagNames = [];
                for (var c = 0; c < $(".tag-checkbox:checked").length; c++) {
                    tagList.push($(".tag-checkbox:checked").eq(c).val());
                    tagNames.push($(".tag-checkbox:checked").eq(c).attr("displayName"));
                }
                $("#tag-input").val(tagList.toString());
                $(".tag-container-form").empty();
                for (var c = 0; c < tagNames.length; c++) {
                    $(".tag-container-form").append("<div>" + tagNames[c] + "</div>");
                }
            });

            $(".dlt-btn").click(function () {
                let id = "#" + $(this).attr('id').replace('s', 'd');
                if (confirm("Deleting comment...")) {
                    $(id).trigger("click");
                }
            });
        });
    });
</script>
</body>
</html>
