<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>
        <c:out value="${sessionScope.user.firstName }"></c:out>
    </title>
</head>
<body>
<div class="">
    <div class="page-title">
        <spring:message code="app.user"/>
    </div>
    <c:if test="${message != null}">
        <div class="alert alert-info max-w-400">
            <c:out value="${message}"></c:out>
        </div>
    </c:if>
    <h1>
        <spring:message code="app.welcome"/> <i><c:out value="${sessionScope.user.firstName }"></c:out></i>
    </h1>

    <div class="row">
        <div class="col-md-6">
            <h5>
                <spring:message code="app.information"/>
            </h5>

            <div class="user-info">
                <spring:message code="app.first.name"/> :
                <c:out value="${user.firstName }"></c:out>
            </div>
            <div class="user-info">
                <spring:message code="app.last.name"/> :
                <c:out value="${user.lastName }"></c:out>
            </div>
            <div class="user-info">
                <spring:message code="app.email"/> :
                <c:out value="${user.email }"></c:out>
            </div>
            <c:if test="${user.address != null && !user.address.isEmpty()}">
                <div class="user-info">
                    <spring:message code="app.address"/> :
                    <c:out value="${user.address}"></c:out>
                </div>
            </c:if>
            <div class="user-info">
                <spring:message code="app.role"/> :
                <c:out value="${user.role.title.replace('_',' ') }"></c:out>
            </div>
            <div class="user-info">
                <spring:message code="app.university"/> :
                <c:out value="${user.university.title}"></c:out>
            </div>
            <div class="user-info">
                <spring:message code="app.account.status"/> :
                <c:out value="${user.accountStatus.displayName}"></c:out>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <a class="btn btn-info" href="profile/changeinfo">
                    <spring:message code="app.change.information"/>
                </a>
            </div>
            <div class="form-group">
                <a class="btn btn-info" href="profile/changepassword">
                    <spring:message code="app.change.password"/>
                </a>
            </div>
        </div>
    </div>
</div>
</body>
</html>
